﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text;
using System.Threading;

using System.Windows.Forms;
using System.IO;
using System.Security;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace CRD_APDU_Tester
{
    public static class THD89_helper
    {

        public static void THD89_return_to_bl_mode()
        {
            int i = 0;
            Common.result_after_issue_cmd = 0x6969;
            if ((Program.mainform.textBox4.InvokeRequired) || (Program.mainform.label19.InvokeRequired))
            {
                Common.myinvoke6 mi = new Common.myinvoke6(THD89_return_to_bl_mode);
                Program.mainform.Invoke(mi);
            }
            else
            {
                Common.flag_pgbl = 1;
                Thread si = new Thread(new ThreadStart(thd89_program_bl_to_SE));
                si.IsBackground = true;
                si.Start();

                for (i = 0; i < 1000; i++)
                {
                    if (1 == Common.pgb_changed)
                    {
                        Common.pgb_changed = 0;
                        i = 0;
                    }
                    if (1 != Common.flag_pgbl) break;

                    Thread.Sleep(10);
                    try
                    {
                        Application.DoEvents();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("i am here");
                    }


                }

            }
        }


        public static void thd89_program_bl_to_SE()
        {
            Common.result_after_issue_cmd = 0x6969;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];


            string app_path = System.Environment.CurrentDirectory;
            string bl_file_name = app_path + "\\binfiles\\" + "returntoBL.txt";

            string[] txt_block = new string[10];
            StreamReader filestream = null;
            int i = 0, j = 0;
            int apdu_count = 0;
            if (File.Exists(bl_file_name))//eclipse_secure_matcher_debug.bin
            {
                filestream = new StreamReader(bl_file_name, System.Text.Encoding.Default);

                while (!filestream.EndOfStream)
                {
                    txt_block[i] = filestream.ReadLine();
                    //Console.WriteLine(txt_block[i]);
                    //Console.WriteLine(txt_block[i].Length);
                    i++;
                }
                apdu_count = i;
                filestream.Close();
                filestream.Dispose();
            }
            else
            {
                //Console.WriteLine("file not found");
                Common.result_after_issue_cmd = 0x6969;
                Common.flag_pgbl = 0;
                return;
            }
            // to do send apdu
            byte[] apdu_test = new byte[264];
            uint apdu_length = 0;
            Common.result_after_issue_cmd = Common.connect_the_card();
            if (0x9000 != Common.result_after_issue_cmd)
            {
                Common.flag_pgbl = 0;
                return;
            }
            for (i = 0; i < apdu_count; i++)
            {
                apdu_length = (uint)(txt_block[i].Length / 2);
                Common.pgb_changed = 1;
                for (j = 0; j < apdu_length; j++)
                {
                    apdu_test[j] = Convert.ToByte(txt_block[i].Substring(j * 2, 2), 16);
                }
                receive_test_length = 264;
                Common.result_after_issue_cmd = Common.common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
                Common.result_after_issue_cmd = Common.check_ret_data(Common.result_after_issue_cmd);
                if (0x9000 != Common.result_after_issue_cmd)
                {
                    Common.flag_pgbl = 0;
                    return;
                }
            }
            Common.PS_to_DBB("Return to BL done\r\n");

            Common.result_after_issue_cmd = Common.Disconnect_the_card((int)Common.Disposition_mode.SCARD_UNPOWER_CARD);
            Common.flag_pgbl = 0;
            return;
        }


        public static void thd89_update_SE_FW()
        {
            int i = 0;
            Common.result_after_issue_cmd = 0x6969;
            if ((Program.mainform.textBox4.InvokeRequired) || (Program.mainform.label19.InvokeRequired))
            {
                Common.myinvoke6 mi = new Common.myinvoke6(thd89_update_SE_FW);
                Program.mainform.Invoke(mi);
            }
            else
            {
                Common.flag_pgb2 = 1;
                Thread si = new Thread(new ThreadStart(thd89_program_SE_FW));
                si.IsBackground = true;
                si.Start();

                for (i = 0; i < 1000; i++)
                {
                    if (1 == Common.pgb_changed)
                    {
                        Common.pgb_changed = 0;
                        i = 0;
                    }
                    if (1 != Common.flag_pgb2) break;

                    Thread.Sleep(10);
                    try
                    {
                        Application.DoEvents();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("i am here");
                    }


                }

            }
        }


        public static void thd89_program_SE_FW()
        {
            Common.result_after_issue_cmd = 0x6969;
            if (!(File.Exists(Common.SE_FW_filename)))//eclipse_secure_matcher_debug.bin
            {
                Common.PS_to_DBB("No SE FW file found\r\n");
                Common.flag_pgb2 = 0;
                return;
            }
            int i = 0, j = 0;
            int apdu_count = 0;
            string temp_string;
            StreamReader filestream = null;
            List<string> txt_block = new List<string>();
            filestream = new StreamReader(Common.SE_FW_filename, System.Text.Encoding.Default);

            while (!filestream.EndOfStream)
            {
                temp_string = filestream.ReadLine();
                temp_string = temp_string.Replace("\n", "").Replace(" ", "").Replace("\t", "").Replace("\r", "").Replace("#", "");
                if ("" != temp_string)
                {
                    txt_block.Add(temp_string);
                    i++;
                }

            }
            apdu_count = i;
            filestream.Close();
            filestream.Dispose();
            //Console.WriteLine(i);
            //todo send all apdu
            byte[] apdu_test = new byte[264];
            uint apdu_length = 0;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            Common.result_after_issue_cmd = Common.connect_the_card();
            if (0x9000 != Common.result_after_issue_cmd)
            {
                Common.flag_pgb2 = 0;
                //Console.WriteLine("11111111111111111111");
                //Console.WriteLine(result_after_issue_cmd);
                return;
            }
            //ui
            int percent_value = 0;


            Program.mainform.update_any_pgb(2, percent_value);
            //ui



            for (i = 0; i < apdu_count; i++)
            {
                apdu_length = (uint)(txt_block[i].Length / 2);
                for (j = 0; j < apdu_length; j++)
                {
                    apdu_test[j] = Convert.ToByte(txt_block[i].Substring(j * 2, 2), 16);
                }
                receive_test_length = 264;
                Common.result_after_issue_cmd = Common.common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
                Common.result_after_issue_cmd = Common.check_ret_data(Common.result_after_issue_cmd);
                if (0x9000 != Common.result_after_issue_cmd)
                {
                    //Console.WriteLine("222222222222222222222");
                    //Console.WriteLine(result_after_issue_cmd);
                    Common.flag_pgb2 = 0;
                    return;
                }

                //ui
                uint temp_data = (uint)((i * 100) / (apdu_count));
                if (percent_value < temp_data)
                {
                    percent_value = (int)temp_data;
                    Common.pgb_changed = 1;
                    Program.mainform.update_any_pgb(2, percent_value);
                }

                //ui
            }
            Common.PS_to_DBB("SE FW update done\r\n");
            Common.result_after_issue_cmd = Common.Disconnect_the_card((int)Common.Disposition_mode.SCARD_UNPOWER_CARD);
            //ui
            percent_value = 100;
            Program.mainform.update_any_pgb(2, percent_value);
            //ui
            Thread.Sleep(1500);
            Common.flag_pgb2 = 0;
            Common.result_after_issue_cmd = 0x9000;
            return;

        }





    }
}
