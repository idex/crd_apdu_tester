﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text;
using System.Threading;

using System.Windows.Forms;
using System.IO;
using System.Security;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace CRD_APDU_Tester
{
    public static class FM1280_helper
    {

        public static void FM_return_to_bl_mode()
        {
            int i = 0;
            Common.result_after_issue_cmd = 0x6969;
            if ((Program.mainform.textBox4.InvokeRequired) || (Program.mainform.label19.InvokeRequired))
            {
                Common.myinvoke6 mi = new Common.myinvoke6(FM_return_to_bl_mode);
                Program.mainform.Invoke(mi);
            }
            else
            {
                Common.flag_pgbl = 1;
                Thread si = new Thread(new ThreadStart(FM1280_return_bl));
                si.IsBackground = true;
                si.Start();

                for (i = 0; i < 1000; i++)
                {
                    if (1 == Common.pgb_changed)
                    {
                        Common.pgb_changed = 0;
                        i = 0;
                    }
                    if (1 != Common.flag_pgbl) break;

                    Thread.Sleep(10);
                    try
                    {
                        Application.DoEvents();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("i am here");
                    }


                }

            }
        }


        public static void FM1280_return_bl()
        {
            Common.result_after_issue_cmd = 0x6969;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = Common.FM1280_bl_apdu;
            Common.result_after_issue_cmd = Common.connect_the_card();
            if (0x9000 != Common.result_after_issue_cmd)
            {
                Common.flag_pgbl = 0;
 
                return;
            }

            receive_test_length = 264;
            Common.result_after_issue_cmd = Common.common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            Common.result_after_issue_cmd = Common.check_ret_data(Common.result_after_issue_cmd);
            if (0x9000 != Common.result_after_issue_cmd)
            {
                Common.flag_pgbl = 0;
                Common.PS_to_DBB(Common.byte_to_hexstr(receive_test, (int)receive_test_length) + "\r\n");
                return;
            }
  
            Common.PS_to_DBB("Return to BL done\r\n");

            Common.result_after_issue_cmd = Common.Disconnect_the_card((int)Common.Disposition_mode.SCARD_UNPOWER_CARD);
            Common.flag_pgbl = 0;
            return;
        }

        public static string FM1280_get_crc(int file_length)
        {
            Common.result_after_issue_cmd = 0x6969;
            if (0 != (file_length % 1024))
            {
                //Console.WriteLine("-----11111");
                return "";
            }
            int k_size = 0;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            List<byte> temp_array_list = new List<byte>(Common.FM1280_get_fw_crc);
            k_size = file_length / 1024;
            if (352 < k_size)
            {
                //Console.WriteLine("-----11111");
                Common.PS_to_DBB("Size over 352K,quit \r\n");
                return "";
            }
            temp_array_list.Add(0x00);
            temp_array_list.Add(Convert.ToByte(k_size));
            byte[] apdu_test = temp_array_list.ToArray();
            Common.result_after_issue_cmd = Common.connect_the_card();
            if (0x9000 != Common.result_after_issue_cmd)
            {
                //Console.WriteLine("-----2222");
                Common.PS_to_DBB("Connect card error \r\n");
                return "";
            }

            receive_test_length = 264;
            Common.result_after_issue_cmd = Common.common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            
            if ((0x90!= receive_test[receive_test_length-2]) ||(0x00 != receive_test[receive_test_length - 1]))
            {
                //Console.WriteLine(receive_test[receive_test_length - 2]);
                //Console.WriteLine(receive_test[receive_test_length - 1]);
                //Console.WriteLine("-----333");
                Common.PS_to_DBB("Read chip CRC error \r\n");
                return "";
            }
            string return_string = "";
            return_string += receive_test[0].ToString("X2");
            return_string += receive_test[1].ToString("X2");
            return return_string;
        }





        public static void FM_update_SE_FW()
        {
            int i = 0;
            Common.result_after_issue_cmd = 0x6969;
            if ((Program.mainform.textBox4.InvokeRequired) || (Program.mainform.label19.InvokeRequired))
            {
                Common.myinvoke6 mi = new Common.myinvoke6(FM_update_SE_FW);
                Program.mainform.Invoke(mi);
            }
            else
            {
                Common.flag_pgb2 = 1;
                Thread si = new Thread(new ThreadStart(FM_program_SE_FW));
                si.IsBackground = true;
                si.Start();

                for (i = 0; i < 1000; i++)
                {
                    if (1 == Common.pgb_changed)
                    {
                        Common.pgb_changed = 0;
                        i = 0;
                    }
                    if (1 != Common.flag_pgb2) break;

                    Thread.Sleep(10);
                    try
                    {
                        Application.DoEvents();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("i am here");
                    }


                }

            }
        }


        public static void FM_program_SE_FW()
        {
            Common.result_after_issue_cmd = 0x6969;
            if (!(File.Exists(Common.SE_FW_filename)))//eclipse_secure_matcher_debug.bin
            {
                Common.PS_to_DBB("No SE FW file found\r\n");
                Common.flag_pgb2 = 0;
                return;
            }
            int i = 0, j = 0;
            int apdu_count = 0;
            string temp_string="";
            string file_crc = "";
            int FW_length = 0;
            StreamReader filestream = null;
            List<string> txt_block = new List<string>();
            filestream = new StreamReader(Common.SE_FW_filename, System.Text.Encoding.Default);
            temp_string = filestream.ReadLine();
            temp_string = temp_string.Replace("\n", "").Replace(" ", "").Replace("\t", "").Replace("\r", "").Replace("#", "");

            try
            {
                FW_length = Convert.ToInt32(temp_string, 16);
                Console.WriteLine(FW_length);
            }
            catch (Exception)
            {
                Console.WriteLine("data is wrong");
                Common.result_after_issue_cmd = 0x6969;
                return;
            }

            
            file_crc = filestream.ReadLine();
            file_crc = file_crc.Replace("\n", "").Replace(" ", "").Replace("\t", "").Replace("\r", "").Replace("#", "");
            //Console.WriteLine("file crc is");
            //Console.WriteLine(file_crc);
            while (!filestream.EndOfStream)
            {
                temp_string = filestream.ReadLine();
                temp_string = temp_string.Replace("\n", "").Replace(" ", "").Replace("\t", "").Replace("\r", "").Replace("#", "");
                if ("" != temp_string)
                {
                    txt_block.Add(temp_string);
                    i++;
                }

            }
            apdu_count = i;
            filestream.Close();
            filestream.Dispose();
            //Console.WriteLine(i);
            //todo send all apdu
            byte[] apdu_test = new byte[264];
            uint apdu_length = 0;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            Common.result_after_issue_cmd = Common.connect_the_card();
            if (0x9000 != Common.result_after_issue_cmd)
            {
                Common.flag_pgb2 = 0;
                //Console.WriteLine("11111111111111111111");
                //Console.WriteLine(result_after_issue_cmd);
                return;
            }
            //ui
            int percent_value = 0;


            Program.mainform.update_any_pgb(2, percent_value);
            //ui



            for (i = 0; i < apdu_count; i++)
            {
                apdu_length = (uint)(txt_block[i].Length / 2);
                for (j = 0; j < apdu_length; j++)
                {
                    apdu_test[j] = Convert.ToByte(txt_block[i].Substring(j * 2, 2), 16);
                }
                receive_test_length = 264;
                Common.result_after_issue_cmd = Common.common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
                Common.result_after_issue_cmd = (uint)(receive_test[receive_test_length-1] + receive_test[receive_test_length-2] * 256);
                if (0x9000 != Common.result_after_issue_cmd)
                {
                    //Console.WriteLine("222222222222222222222");
                    //Console.WriteLine(Common.result_after_issue_cmd);
                    Common.flag_pgb2 = 0;
                    return;
                }

                //ui
                uint temp_data = (uint)((i * 100) / (apdu_count));
                if (percent_value < temp_data)
                {
                    percent_value = (int)temp_data;
                    Common.pgb_changed = 1;
                    Program.mainform.update_any_pgb(2, percent_value);
                }

                //ui
            }



            Common.result_after_issue_cmd = Common.Disconnect_the_card((int)Common.Disposition_mode.SCARD_UNPOWER_CARD);
            //ui
            percent_value = 100;
            Program.mainform.update_any_pgb(2, percent_value);
            //ui
            Thread.Sleep(1500);


            string chip_crc = FM1280_get_crc(FW_length);

            //Console.WriteLine("chip crc is");
            //Console.WriteLine(chip_crc);

            if (file_crc== chip_crc)
            {
                Common.PS_to_DBB("SE FW update done\r\n");
                Common.PS_to_DBB("FW crc is: " + chip_crc + "\r\n");
                Common.result_after_issue_cmd = 0x9000;
            }
            else
            {

                Common.PS_to_DBB("file crc is: "+file_crc+"\r\n");
                Common.PS_to_DBB("chip crc is: " + chip_crc + "\r\n");
                Common.result_after_issue_cmd = 0x6969;
            }

            Common.flag_pgb2 = 0;
            return;

        }





    }
}
