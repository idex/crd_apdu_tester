﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text;
using System.Threading;

using System.Windows.Forms;
using System.IO;
using System.Security;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace CRD_APDU_Tester
{
    static class Common
    {
        public static List<string> SE_list = new List<string> {"CS2_THD89","CS1_SLE78" };
        public static List<string> Format_list = new List<string>
        {
            "Default",
            "BIN",
            "TXT",
            "BMP"
        };
        public static List<string> CMD_list = new List<string>{"None",
            "Get Attrib String",
            "Return BL mode",
            "Program_update_bin",
            "Program_se",
            "Update_BMCU_FW",
            "Update_sensor_fw",
            "BMCU_integration_update",
            "Contacted initial test",
            "Contactless initial test",
            "No sensor cd initial test",
            "No sensor cl initial test",
            "capture image wff",
            "capture image live",
            "Display image wff",
            "Display image live",
            "Demo 1 finger enrollment",
            "Demo 2 finger enrollment",
            "Demo single enrollment",
            "List all record",
            "Demo verify contact",
            "Demo verify contactless",
            "Demo secure verify contact",
            "Demo secure verify contactless",
            "Delete record",
            "Delete single template",
            "Reset key",
            "Version check",
            "output image from card",
            "load image to card",
            "verify load image",
            "Force calibration",
            "Force calibration and store",
            "Get ID",
            "set factory keys",
            "set_factory_keys_V256",
            "open SCP03",
            "open_SCP03_v256",
            "FM1280 return BL",
            "FM1280 return app",
            "FM1280 program SE",
            "IMM_get_template",
            "IMM_merge_template",
            "IMM_store_template",
            "IMM_list_record",
            "IMM_verify_template",
            "FM1280_read_enroll_template",
            "FM1280_read_verify_template",
            "fm1280_temo_4k_data",
            "fm1280_6k_data_after_index_table",
            "fm1280_4k_ram1_after_match",
            "read_data_from_flash",
            "erase_debug_flash_block"

        };

        public struct SCARD_IO_REQUEST
        {
            public uint dwProtocol;
            public uint cbPciLength;
        }
        public struct apdu_rs
        {
            public byte[] data;
            public byte sw1;
            public byte sw2;
        }
        public enum scard_use_mode
        {
            SCARD_SHARE_EXCLUSIVE = 1,
            SCARD_SHARE_SHARED = 2,
            SCARD_SHARE_DIRECT = 3
        }
        public enum scard_protocol_mode
        {
            SCARD_PROTOCOL_T0 = 1,//（使用T = 0协议）
            SCARD_PROTOCOL_T1 = 2//（使用T=1协议）
        }
        public static SCARD_IO_REQUEST current_SC_IO_REQ = new SCARD_IO_REQUEST();
        public static SCARD_IO_REQUEST rev_SC_IO_REQ = new SCARD_IO_REQUEST();
        public static uint result_after_issue_cmd = 0x6969;
        public static int SE_category = 0;//0x00 thd89,0x01,SLE78, other reserve
        public static string textbox4_new_string;
        public static int status_in_process = 0;
        public static int current_percent_value = 0;
        public static int old_percent_value = 0;
        public static int flag_pgb3 = 0;
        public static uint SCARD_ATTR_ATR_STRING = 0x00090303;
        public static uint SCARD_ATTR_VENDOR_NAME = 0x00010100;
        public static int pgb_changed = 0;
        public static int Initialize_status_value = 0;
        public static byte[] mcu_fw_hash = new byte[16];
        public static byte[] sensor_pri_fw_hash = new byte[16];
        public static byte[] sensor_uti_fw_hash = new byte[16];
        public static string SE_FW_filename;
        public static string update_bin_filename;
        public static string mcu_fw_filename;
        public static string sensor_pri_fw_filename;
        public static string sensor_uti_fw_filename;
        public static string load_image_filename;
        public static string load_FW_filename;
        public static active_card current_card = new active_card();

        public static List<string> CMD_list_m1;
        public static List<string> CMD_list_m2;
        public static List<string> CMD_list_m3;
        public static List<string> CMD_list_m4;
        public static List<string> CMD_list_m5;
        public static List<string> CMD_list_m6;
        public static List<string> CMD_list_m7;
        public static List<string> CMD_list_m8;
        public static List<string> CMD_list_m9;
        public static List<string> Format_list_m1;


        public static readonly byte[] initialize = { 0x00, 0x54, 0x00, 0x00, 0x01 };    // + 0x0x bit0,contact/contactless, bit1, sensor initial yes/no
        public static readonly byte[] uninitialize = { 0x00, 0x54, 0x01, 0x00, 0x00 };
        public static readonly byte[] get_version = { 0x00, 0x54, 0x30, 0x00, 0x00 };
        public static readonly byte[] get_fw_version = { 0x00, 0x54, 0x36, 0x00, 0x00 };
        public static readonly byte[] get_UID = { 0x00, 0x54, 0x24, 0x00, 0x01 };//+0x00:mcu id, +0x01:sensor id, +0x02:sensor M info
        public static readonly byte[] prepare_image = { 0x00, 0x54, 0x02, 0x00, 0x01 };
        public static readonly byte[] get_image = { 0x00, 0x54, 0x04, 0x10, 0x01, 0x00 };
        public static readonly byte[] load_image = { 0x00, 0x54, 0x52 };
        public static readonly byte[] read_data = { 0x00, 0x54, 0x50, 0x01, 0x04 };
        public static readonly byte[] imm_match_templates = { 0x00, 0x54, 0x07, 0x00,0x02,0x0e,0x00 };
        public static readonly byte[] imm_merge_templates = { 0x00, 0x54, 0x06, 0x00,0x02,0x03,0x01 };
        public static readonly byte[] imm_get_template = { 0x00, 0x54, 0x05, 0x00, 0x01, 0x00 };
        public static readonly byte[] imm_store_template = { 0x00, 0x54, 0x10, 0x00, 0x08, 0x01, 0x00, 0x00,0x00,0x00,0x00,0x00,0x00 };// special code for tests
        public static readonly byte[] imm_list_records = { 0x00, 0x54, 0x12, 0x00, 0x03, 0xff, 0xff, 0xff };
        public static readonly byte[] list_records = { 0x00, 0x54, 0x12, 0x00, 0x03, 0x40, 0xff, 0xff };
        public static readonly byte[] delete_record = { 0x00, 0x54, 0x11, 0x00, 0x02, 0xff, 0xff };
        public static readonly byte[] delete_template = { 0x00, 0x54, 0x11, 0x00, 0x02 };
        public static readonly byte[] store_record = { 0x00, 0x54, 0x10 };
        public static readonly byte[] set_parameter_32 = { 0x00, 0x54, 0x09, 0x01, 0x03, 0x21, 0x8c, 0x0a };
        public static readonly byte[] get_parameter_32 = { 0x00, 0x54, 0x08, 0x01, 0x01, 0x21 };
        public static readonly byte[] store_params = { 0x00, 0x54, 0x0c, 0x01, 0x00 };
        public static readonly byte[] Reset_key = { 0x00, 0x54, 0x21, 0x00, 0x00 };
        public static readonly byte[] calibrate = { 0x00, 0x54, 0x45, 0x00, 0x01 };// +0x01 only calibrate +0x09 calibrate and store
        public static readonly byte[] verify_cd_live = { 0x00, 0x55, 0x02, 0x01, 0x00 };
        public static readonly byte[] verify_cd_wff = { 0x00, 0x55, 0x02, 0x03, 0x00 };
        public static readonly byte[] verify_cl_live = { 0x00, 0x55, 0x03, 0x01, 0x00 };
        public static readonly byte[] verify_cl_wff = { 0x00, 0x55, 0x03, 0x03, 0x00 };
        // update commands
        public static readonly byte[] store_blob = { 0x00, 0x54, 0x18 };
        public static readonly byte[] update_start = { 0x00, 0x54, 0x31, 0x00, 0x00 };
        public static readonly byte[] update_details = { 0x00, 0x54, 0x32, 0x00 };
        public static readonly byte[] update_data = { 0x00, 0x54, 0x33, 0x00 };
        public static readonly byte[] update_hash = { 0x00, 0x54, 0x34, 0x00 };
        public static readonly byte[] update_end = { 0x00, 0x54, 0x35, 0x00, 0x00 };
        public static readonly byte[] SE_EMK_enroll = { 0x00, 0x54, 0x87, 0x16, 0x00 };
        public static readonly byte[] SE_EMK_single_enroll = { 0x00, 0x54, 0x8a };
        public static readonly byte[] SE_EMK_enroll1 = { 0x00, 0x54, 0x8b, 0x01 };
        public static readonly byte[] SE_EMK_enroll2 = { 0x00, 0x54, 0x8b, 0x02 };
        public static readonly byte[] SE_EMK_match = { 0x00, 0x54, 0x88, 0x26, 0x03,0x01,0xde,0x1e };
        public static readonly byte[] SE_EMK_secure_match = { 0x00, 0x54, 0x88, 0x00, 0x03,0x00,0xde,0x1e };
        public static readonly byte[] SE_EMK_verify_load_image = { 0x00, 0x54, 0x88, 0x26, 0x01, 0x00 };
        public static readonly byte[] SE_EMK_info = { 0x00, 0x54, 0x89, 0x00, 0x00 };
        public static readonly byte[] SE_EMK_debug = { 0x00, 0x54, 0x8c, 0x00, 0x00 };

        public static readonly byte[] FM1280_bl_apdu = { 0x00, 0x00, 0x00, 0x00, 0x00 };
        public static readonly byte[] FM1280_app_apdu = { 0xf0, 0x0a, 0x00, 0x08, 0x04, 0x00, 0x00, 0x00, 0x00 };
        public static readonly byte[] FM1280_get_fw_crc = { 0xF0,0x1C,0x00,0x00,0x06,0x00,0x00,0x00,0x00 };
        public static readonly byte[] FM1280_read_template = { 0x00,0x04,0x00,0x00,0x00};

        public static readonly byte[] erase_flash_debug_block = { 0x00, 0x54, 0x70, 0x00, 0x00 };
        public static readonly byte[] read_data_from_flash = { 0x00, 0x54, 0x71, 0x00, 0x04,0x00,0x00,0x00,0x00 };

        [DllImport(dllName: "\\dll\\idx_serial_stack_x86.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "cmac_compute")]
        public static extern void cmac_compute(ref byte key, ref byte buffer, int len, ref byte output);

        //64 bit define:
        /*
        [DllImport("WinScard.dll")]
        public static extern int SCardEstablishContext(uint dwScope, int nNotUsed1, int nNotUsed2, ref ulong phContext);
        [DllImport("WinScard.dll")]
        public static extern int SCardReleaseContext(ulong phContext);
        [DllImport("WinScard.dll")]
        public static extern int SCardConnect(ulong hContext, string cReaderName, uint dwShareMode, uint dwPrefProtocol, ref ulong phCard, ref uint ActiveProtocol);
        [DllImport("WinScard.dll")]
        public static extern int SCardDisconnect(ulong hCard, int Disposition);
        [DllImport("WinScard.dll")]
        public static extern int SCardListReaders(ulong hContext, byte[] cGroups, byte[] cReaderLists, ref uint nStringSize);
        [DllImport("WinScard.dll")]
        public static extern int SCardFreeMemory(ulong hContext, string cResourceToFree);
        [DllImport("WinScard.dll")]
        public static extern int SCardGetAttrib(ulong hContext, uint dwAttrId, [Out] byte[] bytRecvAttr, out int nRecLen);
        [DllImport("WinScard.dll")]
        public static extern uint SCardTransmit(ulong hCard, [In] ref SCARD_IO_REQUEST pioSendPci, byte[] pbSendbuff, uint cbSendLength, ref SCARD_IO_REQUEST pioRecvPci, [Out] byte[] pbRecvbuff, out uint pcbRecvLength);
        [DllImport("WinScard.dll")]
        public static extern int SCardStatus(ulong hCard, string mszReaderNames, ref uint pcchReaderLen, ref uint pdwState, ref uint pdwProtocol, ref byte[] pbAtr, ref uint pbcAtrLen);

        */

        //32 bit

        [DllImport("WinScard.dll")]
        public static extern int SCardEstablishContext(uint dwScope, int nNotUsed1, int nNotUsed2, ref uint phContext);
        [DllImport("WinScard.dll")]
        public static extern int SCardReleaseContext(uint phContext);
        [DllImport("WinScard.dll")]
        public static extern int SCardConnect(uint hContext, string cReaderName, uint dwShareMode, uint dwPrefProtocol, ref uint phCard, ref uint ActiveProtocol);
        [DllImport("WinScard.dll")]
        public static extern int SCardDisconnect(uint hCard, int Disposition);
        [DllImport("WinScard.dll")]
        public static extern int SCardListReaderGroups(uint hContext, byte[] cGroups, ref int nStringSize);
        [DllImport("WinScard.dll")]
        public static extern int SCardListReaderGroups(ulong hContext, byte[] cGroups, ref int nStringSize);
        [DllImport("WinScard.dll")]
        public static extern int SCardListReaders(uint hContext, byte[] cGroups, byte[] cReaderLists, ref uint nStringSize);
        [DllImport("WinScard.dll")]
        public static extern int SCardFreeMemory(uint hContext, string cResourceToFree);
        [DllImport("WinScard.dll")]
        public static extern int SCardTransmit(uint hCard, [In] ref SCARD_IO_REQUEST pioSendPci, byte[] pbSendBuffer, uint cbSendLength, ref SCARD_IO_REQUEST pioRecvPci, [Out] byte[] pbRecvBuffer, out uint pcbRecvLength);
        [DllImport("WinScard.dll")]
        public static extern int SCardGetAttrib(uint hCard, uint dwAttrId, [Out] byte[] bytRecvAttr, out uint nRecLen);
        [DllImport("WinScard.dll")]
        public static extern int SCardStatus(uint hCard, string mszReaderNames, ref uint pcchReaderLen, ref uint pdwState, ref uint pdwProtocol, [Out] byte[] pbAtr, ref uint pbcAtrLen);




        public static uint get_file_crc32(string filename)
        {
            byte[] binData;
            int file_lenth = 0;
            uint crc_data = 0;
            if (File.Exists(filename))//eclipse_secure_matcher_debug.bin
            {
                FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                file_lenth = (int)fs.Length;
                binData = br.ReadBytes(file_lenth);
                fs.Close();
            }
            else
            {
                Console.WriteLine("file not found");
                return 0;
            }

            crc_data = IDEX_CRC32(binData, (uint)file_lenth);

            return crc_data;
        }

        public static string byte_to_hexstr(byte[] data_array, int length)
        {
            string temp_string = "";
            if (data_array != null)
            {
                for (int i = 0; i < length; i++)
                {
                    temp_string += data_array[i].ToString("X2");
                }
                return temp_string;
            }
            return "";
        }


        public static uint IDEX_CRC32([In] byte[] data, [In] uint data_size)
        {
            uint POLYNOMIAL = 0xEDB88320;
            uint crc = 0xFFFFFFFF;
            uint datatemp = 0;
            for (int i = 0; i < (data_size / 4); i++)
            {
                datatemp = 0;
                datatemp += data[i * 4 + 3];
                datatemp <<= 8;
                datatemp += data[i * 4 + 2];
                datatemp <<= 8;
                datatemp += data[i * 4 + 1];
                datatemp <<= 8;
                datatemp += data[i * 4];
                crc = crc ^ datatemp;
                for (int j = 0; j < 32; j++)
                {
                    crc >>= 1;
                    if (0 != (crc & 0x00000001))
                    {
                        crc = crc ^ POLYNOMIAL;
                    }

                }
            }
            crc ^= 0xFFFFFFFF;
            return crc;
        }

        public static void get_hash(string filename, byte[] hash_data)
        {
            byte[] binData;
            int file_lenth = 0;
            if (File.Exists(filename))//eclipse_secure_matcher_debug.bin
            {
                FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                file_lenth = (int)fs.Length;
                binData = br.ReadBytes(file_lenth);
                fs.Close();
            }
            else
            {
                Console.WriteLine("file not found");
                return;
            }

            byte[] key_buff = new byte[16];
            byte[] hash_buff = new byte[16];
            for (int i = 0; i < 16; i++)
            {
                key_buff[i] = 0x0a;
                hash_buff[i] = 0x00;
            }
            cmac_compute(ref key_buff[0], ref binData[0], file_lenth, ref hash_buff[0]);
            for (int i = 0; i < 16; i++)
            {
                hash_data[i] = hash_buff[i];
            }

        }



        public static uint get_hcontext()
        {
            uint m_hContext = 0;
            int lReturn;
            lReturn = SCardEstablishContext(0, 0, 0, ref m_hContext);
            if (lReturn < 0)
            {
                Console.WriteLine("setup PCSC driver failed");
                return 0;
            }
            //Console.WriteLine(m_hContext);
            return m_hContext;
        }



        public static List<string> get_reader_list(uint m_hContext)
        {

            uint cch = 0;
            int lReturn;
            //uint m_hContext=0;
            byte[] read_data;
            string reader_group;
            List<string> list = new List<string>();
            List<string> orignal_list = new List<string>();

            //Console.WriteLine(m_hContext);

            lReturn = SCardListReaders(m_hContext, null, null, ref cch);
            if (0 == lReturn)
            {
                read_data = new byte[cch];
                lReturn = SCardListReaders(m_hContext, null, read_data, ref cch);
                if (0 == lReturn)
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    reader_group = encoding.GetString(read_data);
                    //Console.WriteLine(reader_group);
                    int index = 0;
                    char nullchar = '\0';
                    int len = (int)cch;
                    while (reader_group[0] != nullchar)//分割读卡器列表
                    {
                        index = reader_group.IndexOf(nullchar);
                        string reader = reader_group.Substring(0, index);
                        list.Add(reader);
                        len = len - (reader.Length + 1);
                        reader_group = reader_group.Substring(index + 1, len);
                    }
                    //return list;
                    index = 0;
                    int name_lenth = 0;
                    int i = 0;
                    //byte[] temp_data = new byte[len];
                    while (i < cch)//分割读卡器列表
                    {
                        if (nullchar == read_data[i])
                        {
                            if (i == (cch - 1)) break;
                            name_lenth = i - index;
                            //byte[] temp_data = new byte[name_lenth];
                            reader_group = "";
                            for (int j = 0; i < name_lenth; j++)
                            {
                                //temp_data[j] = read_data[index + j];
                                reader_group += read_data[index + j];
                            }
                            //reader_group = encoding.GetString(temp_data);
                            orignal_list.Add(reader_group);
                            index = i + 1;
                            i++;
                        }
                        else
                        {
                            i++;
                        }

                    }
                    //Console.WriteLine("ol count:{0}",orignal_list.Count);
                    //byte[] temp_data = new byte[orignal_list[0].Length];
                    //temp_data = System.Text.Encoding.Default.GetBytes(orignal_list[0]);
                    //Console.WriteLine("l1:{0}", encoding.GetString(temp_data));
                    //Console.WriteLine("l1:{0}", orignal_list[1]);

                }
                else
                {
                    PS_to_DBB("no reader\r\n");
                    //return null;
                    list = null;
                    orignal_list = null;
                }


            }
            else
            {
                PS_to_DBB("no reader\r\n");
                //return null;
                list = null;
                orignal_list = null;
            }
            //lReturn = SCardReleaseContext(m_hContext);
            //Console.WriteLine(m_hContext);
            return list;
            //return orignal_list;
        }


        public static void print_data_to_debugbox(int data)
        {
            byte[] temp_bytes = BitConverter.GetBytes(data);
            string temp_string = string.Empty;
            for (int i = temp_bytes.Length - 1; i >= 0; i--)
            {
                temp_string += temp_bytes[i].ToString("X2");
            }
            PS_to_DBB("0X" + temp_string);
            //Application.DoEvents();
        }
        public struct active_card
        {
            public uint hContext;
            public string cReaderName;
            public uint dwShareMode;
            public uint dwPrefProtocol;
            public uint phCard;
            public uint ActiveProtocol;
        }

        public enum Disposition_mode
        {
            SCARD_LEAVE_CARD = (int)0,
            SCARD_RESET_CARD = (int)1,
            SCARD_UNPOWER_CARD = (int)2,
            SCARD_EJECT_CARD = (int)3

        }
        public static void initial_card_driver(uint hContext, ref active_card smart_card)
        {
            smart_card.hContext = hContext;
            smart_card.dwShareMode = (uint)scard_use_mode.SCARD_SHARE_SHARED;
            smart_card.dwPrefProtocol = (uint)scard_protocol_mode.SCARD_PROTOCOL_T0;
            smart_card.phCard = 0;
            smart_card.ActiveProtocol = 0;
        }


        public static string search_card(List<string> reader_list, ref active_card smart_card)
        {
            int ret = 0;
            int index = 0;
            if (0 < reader_list.Count)
            {
                for (int i = 0; i < reader_list.Count; i++)
                {
                    ret = SCardConnect(smart_card.hContext, reader_list[i], smart_card.dwShareMode, (uint)scard_protocol_mode.SCARD_PROTOCOL_T0, ref smart_card.phCard, ref smart_card.ActiveProtocol);
                    //Console.WriteLine("reader{0}result{1:x}",i,ret);
                    //Console.WriteLine("protocol{0}", smart_card.ActiveProtocol);
                    if (0 == ret)
                    {
                        index = i;
                        break;
                    }
                    if (0x8010000f == (uint)ret)
                    {
                        ret = SCardConnect(smart_card.hContext, reader_list[i], smart_card.dwShareMode, (uint)scard_protocol_mode.SCARD_PROTOCOL_T1, ref smart_card.phCard, ref smart_card.ActiveProtocol);
                        //Console.WriteLine("reader{0}result{1:x}", i, ret);
                        //Console.WriteLine("protocol{0}", smart_card.ActiveProtocol);
                        if (0 == ret)
                        {
                            index = i;
                            break;
                        }
                    }
                }

                smart_card.cReaderName = reader_list[index];
                //Console.WriteLine("readernamesearch:{0}",smart_card.cReaderName);
                //Console.WriteLine("phCardsearch:{0}", smart_card.phCard);
                //Console.WriteLine("ActiveProtocolsearch:{0}", smart_card.ActiveProtocol);
                return smart_card.cReaderName;
            }

            return null;
        }

        public static uint Disconnect_the_card(int mode)
        {
            uint ret_data = 0x9000;
            ret_data = (uint)SCardDisconnect(current_card.phCard, mode);
            if (0 != ret_data)
            {
                PS_to_DBB("Reset SE failed\r\n");
                return ret_data;
            }
            return 0x9000;
        }


        public static uint connect_the_card()
        {
            string active_reader_name;
            List<string> reader_list = new List<string>();
            uint m_hContext = 0;
            m_hContext = get_hcontext();
            if (0 == m_hContext) return 0x6969;
            reader_list = get_reader_list(m_hContext);
            if (null == reader_list) return 0x6969;
            if (0 >= reader_list.Count) return 0x6969;


            initial_card_driver(m_hContext, ref current_card);
            active_reader_name = search_card(reader_list, ref current_card);

            if (true == Program.mainform.checkBox1.Checked)
            {
                PS_to_DBB("Total reader num is: ");
                print_data_to_debugbox(reader_list.Count);
                PS_to_DBB("\r\n");
                for (int i = 0; i < reader_list.Count; i++)
                {
                    PS_to_DBB(reader_list[i] + "\r\n");
                }
                PS_to_DBB("Current reader:" + current_card.cReaderName + "\r\n");
                //print_data_to_debugbox((int)current_card.phCard);
                //print_data_to_debugbox((int)current_card.ActiveProtocol);
            }

            current_SC_IO_REQ.dwProtocol = current_card.ActiveProtocol;
            current_SC_IO_REQ.cbPciLength = 8;
            rev_SC_IO_REQ.dwProtocol = 0;
            rev_SC_IO_REQ.cbPciLength = 8;
            return 0x9000;
        }

        public static void show_Reader_response(byte[] response, int length)
        {
            if (0 == length)
            {
                PS_to_DBB("no response\r\n");
                return;
            }
            string temp_string = byte_to_hexstr(response, length);
            PS_to_DBB(temp_string + "\r\n");
            return;
        }


        public static uint common_cd_ini_test(byte mode)
        {
            uint ret_data = 0x6969;
            ret_data=cd_ini_test(mode);
            return ret_data;
        }

        public static uint common_cd_un_ini_test()
        {
            uint ret_data = 0x6969;
            ret_data = cd_un_ini_test();
            return ret_data;
        }

        public static uint common_send_apdu([In]byte[] apdu, [In] uint apdu_length, ref byte[] response, ref uint response_length)
        {
            uint ret_data = 0x6969;
            ret_data = send_apdu(apdu, apdu_length, ref response, ref response_length);
            return ret_data;
        }

        public static uint check_ret_data(uint ret)
        {
            uint ret_data = 0x6969;
            //Console.WriteLine(ret);
            byte[] temp_array = new byte[4];
            ret_data = ret;
            if (1 == SE_category)
            {
                temp_array = BitConverter.GetBytes(ret);
                ret_data = (uint)(temp_array[1]+ (temp_array[0]*256));
            }
            //Console.WriteLine(ret_data);
            return ret_data;
        }

        public static uint get_response_data_length(uint length)
        {
            uint ret_data = length;
            //Console.WriteLine(length);
            byte[] temp_array = new byte[4];
            ret_data = length;
            if (1 == SE_category)
            {
                temp_array = BitConverter.GetBytes(length);
                ret_data = (uint)(temp_array[1] + (temp_array[0] * 256));
            }
            //Console.WriteLine(ret_data);
            return ret_data;
        }

        public static uint cd_ini_test(byte mode)
        {
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            uint ret_data = 0x6969;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            List<byte> temp_array_list = new List<byte>(initialize);
            temp_array_list.Add(mode);

            byte[] apdu_test = temp_array_list.ToArray();
            //Console.WriteLine(apdu_test.Length);
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data=check_ret_data(ret_data);
            if (0x9000 != ret_data) return ret_data;
            Initialize_status_value = (mode | 0x02);
            return ret_data;
        }

        public static uint cd_un_ini_test()
        {
            if (Initialize_status_value == 0) return 0x9000;
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = uninitialize;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            if (0x9000 != ret_data) return ret_data;

            Initialize_status_value = 0;

            return ret_data;
        }


        public static uint send_apdu([In]byte[] apdu, [In] uint apdu_length, ref byte[] response, ref uint response_length)
        {
            int ret = 0;
            uint received_length = 264;
            byte[] received_buff = new byte[264];
            string temp_string = "";
            if (true == Program.mainform.checkBox1.Checked)
            {
                temp_string = byte_to_hexstr(apdu, (int)apdu_length);
                Program.mainform.print_string_to_debugbox("Send:" + temp_string + "\r\n");
            }
            ret = SCardTransmit(current_card.phCard, ref current_SC_IO_REQ, apdu, apdu_length, ref rev_SC_IO_REQ, received_buff, out received_length);//ref rev_SC_IO_REQ
            //Console.WriteLine("ret:{0:x}", ret);
            //Console.WriteLine("received lenth:{0}", received_length);
            if (ret != 0)
            {
                Console.WriteLine(ret);
                return 0x6969;
            }
            response_length = received_length;
            for (int i = 0; i < received_length; i++)
            {
                response[i] = received_buff[i];
            }
            if (true == Program.mainform.checkBox1.Checked)
            {
                temp_string = byte_to_hexstr(response, (int)response_length);
                Program.mainform.print_string_to_debugbox("Response:" + temp_string + "\r\n");
            }
            if (2 <= response_length)
            {
                ret = (response[response_length - 2] * 256 + response[response_length - 1]);
            }
            else
            {
                //PS_to_DBB("111111111111\r\n");
                return 0x6969;
            }

            if(0x61 == response[response_length - 2])
            { 
            while (0x61 == response[response_length - 2])
            {

                byte[] temp_apdu = new byte[5] { 0x00, 0xc0, 0x00, 0x00, 0x00 };
                temp_apdu[4] = response[response_length - 1];
                received_length = Convert.ToUInt32(temp_apdu[4] + 2);
                //Console.WriteLine("length=");
                //Console.WriteLine(received_length);
                if (true == Program.mainform.checkBox1.Checked)
                {
                    temp_string = byte_to_hexstr(temp_apdu, (int)temp_apdu.Length);
                    Program.mainform.print_string_to_debugbox("Send:" + temp_string + "\r\n");
                }
                ret = SCardTransmit(current_card.phCard, ref current_SC_IO_REQ, temp_apdu, (uint)temp_apdu.Length, ref rev_SC_IO_REQ, received_buff, out received_length);//ref rev_SC_IO_REQ
                if (ret != 0)
                {
                    //PS_to_DBB("222222222222\r\n");
                    //Console.WriteLine(ret);
                    return 0x6969;
                }
                response_length = received_length;
                for (int i = 0; i < received_length; i++)
                {
                    response[i] = received_buff[i];
                }
                if (true == Program.mainform.checkBox1.Checked)
                {

                    temp_string = byte_to_hexstr(response, (int)response_length);
                    Program.mainform.print_string_to_debugbox("Response:" + temp_string + "\r\n");
                }
                //Console.WriteLine(response_length);
                //Console.WriteLine(received_buff[0]);
                if (0x61 == received_buff[response_length - 2]) continue;
                if (2 <= response_length)
                {
                    ret = (response[response_length - 2] * 256 + response[response_length - 1]);
                    //PS_to_DBB("3333333333333\r\n");
                    return (uint)ret;
                }
                else
                {
                    //PS_to_DBB("444444444444\r\n");
                    return 0x6969;
                }
            }
        }
            if (0x6c == response[response_length - 2])
            {
                
                received_length =  Convert.ToUInt32(response[response_length - 1] + 2);
                apdu[4] = response[response_length - 1];
                if (true == Program.mainform.checkBox1.Checked)
                {
                    temp_string = byte_to_hexstr(apdu, (int)apdu.Length);
                    Program.mainform.print_string_to_debugbox("Send:" + temp_string + "\r\n");
                }
                ret = SCardTransmit(current_card.phCard, ref current_SC_IO_REQ, apdu, (uint)apdu.Length, ref rev_SC_IO_REQ, received_buff, out received_length);//ref rev_SC_IO_REQ
                if (ret != 0) return 0x6969;
                response_length = received_length;
                for (int i = 0; i < received_length; i++)
                {
                    response[i] = received_buff[i];
                }
                if (true == Program.mainform.checkBox1.Checked)
                {

                    temp_string = byte_to_hexstr(response, (int)response_length);
                    Program.mainform.print_string_to_debugbox("Response:" + temp_string + "\r\n");
                }
                if (2 <= response_length)
                {
                    ret = (response[response_length - 2] * 256 + response[response_length - 1]);
                    //Console.WriteLine("6666666666666");
                    //return (uint)ret;
                }
                else
                {
                    //PS_to_DBB("555555555555\r\n");
                    return 0x6969;
                }
            }
            if (0x9000 != ret) return (uint)ret;
            if (4 <= response_length)
            {
                ret = (response[response_length-2] * 256 + response[response_length-1]);
            }

            return (uint)ret;



        }


        public static uint get_sensor_version(ref string version)
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = get_fw_version;
            uint temp_data = 0;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            if (0 == Initialize_status_value)
            {
                Initialize_status_value = 2;
                ret_data = common_cd_ini_test(2);
                if (0x9000 != ret_data) return ret_data;
            }
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            if (0x9000 != ret_data) return ret_data;
            temp_data = (uint)(receive_test[12] + receive_test[13] * 256);
            string temp_string = receive_test[10].ToString() + "." + receive_test[11].ToString() + "." + temp_data.ToString();
            version = temp_string;

            return ret_data;
        }
        public static uint get_mcu_version(ref string version)
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = get_version;
            uint temp_data = 0;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            if (0 == Initialize_status_value)
            {
                Initialize_status_value = 2;
                ret_data = common_cd_ini_test(2);
                if (0x9000 != ret_data) return ret_data;
            }
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            if (0x9000 != ret_data) return ret_data;
            temp_data = (uint)(receive_test[12] + receive_test[13] * 256);
            string temp_string = receive_test[10].ToString() + "." + receive_test[11].ToString() + "." + temp_data.ToString();
            version = temp_string;

            return ret_data;
        }

        public static uint get_scard_atr()
        {
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            uint ret_data = 0x6969;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;

            ret_data = (uint)SCardGetAttrib(current_card.phCard, SCARD_ATTR_ATR_STRING, receive_test, out receive_test_length);
            if (0 != ret_data) return 0x6969;
            show_Reader_response(receive_test, (int)receive_test_length);
            return 0x9000;
        }



        public static uint version_check()
        {
            uint ret_data = 0x6969;
            uint over_all_result = 0;
            string target_data = "";
            string real_data = "";

            if (true == Program.mainform.checkBox3.Checked)
            {
                PS_to_DBB("SE version was not supported so far\r\n");
                over_all_result += 1;
            }
            if (true == Program.mainform.checkBox4.Checked)
            {
                target_data = Program.mainform.textBox15.Text;
                ret_data = get_mcu_version(ref real_data);
                if (0x9000 != ret_data) return ret_data;
                Program.mainform.textBox12.Text = real_data;
                Program.mainform.textBox12.Update();
                PS_to_DBB("MCU target Version:" + target_data + "\r\n");
                PS_to_DBB("MCU real Version:" + real_data + "\r\n");
                if (target_data != real_data)
                {
                    PS_to_DBB("MCU version is not match\r\n");
                    over_all_result += 1;
                }

            }
            if (true == Program.mainform.checkBox5.Checked)
            {
                target_data = Program.mainform.textBox17.Text;
                ret_data = get_sensor_version(ref real_data);
                if (0x9000 != ret_data) return ret_data;
                Program.mainform.textBox16.Text = real_data;
                Program.mainform.textBox16.Update();
                PS_to_DBB("SENSOR target Version:" + target_data + "\r\n");
                PS_to_DBB("SENSOR real Version:" + real_data + "\r\n");
                if (target_data != real_data)
                {
                    PS_to_DBB("SENSOR version is not match\r\n");
                    over_all_result += 1;
                }
            }
            if (0 < over_all_result)
            {
                PS_to_DBB(over_all_result.ToString() + " items of version test failed\r\n");
                return 0x6969;
            }

            return 0x9000;
        }


        public static byte[] get_image_array_from_image(string image_name)
        {
            byte[] image_array = null;
            int i = 0;
            int j = 0;
            if (false == File.Exists(image_name))
            {
                PS_to_DBB("File does not exist.\r\n");
                return image_array;
            }
            Bitmap test_image = new Bitmap(image_name);
            Rectangle rect = new Rectangle(0, 0, test_image.Width, test_image.Height);
            if ((132 < test_image.Width) || (132 < test_image.Height)) return image_array;

            //Console.WriteLine(test_image.Width);
            //Console.WriteLine(test_image.Height);

            //System.Drawing.Imaging.BitmapData test_bmpdata = test_image.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadOnly, test_image.PixelFormat);
            byte[] temp_array = new byte[20000];
            //IntPtr ptr = test_bmpdata.Scan0;
            //int bytes = Math.Abs(test_bmpdata.Stride) * test_image.Height;
            //Console.WriteLine(test_image.GetPixel(55,55).R);

            //System.Runtime.InteropServices.Marshal.Copy(ptr, temp_array, 0, bytes);
            //test_image.UnlockBits(test_bmpdata);
            image_array = new byte[test_image.Width * test_image.Height];
            //uint offset = (uint)(test_bmpdata.Stride - test_image.Width);
            uint dist_ptr = 0;
            //uint surc_ptr = 0;

            if (true == Program.mainform.checkBox2.Checked)
            {
                for (i = test_image.Height-1; i >= 0; i--)
                {
                    for (j = 0; j < test_image.Width; j++)
                    {
                        image_array[dist_ptr++] = test_image.GetPixel(j, i).R;
                    }

                }
            }
            else
            {
                for (i = 0; i < test_image.Height; i++)
                {
                    for (j = 0; j < test_image.Width; j++)
                    {
                        image_array[dist_ptr++] = test_image.GetPixel(j, i).R;
                    }

                }
            }

            return image_array;
        }

        public static byte[] get_image_from_array(byte[] image_array, int width, int height)
        {
            int i = 0;
            if (width * height > 17292) return null;
            byte[] image_flow = new byte[14 + 40 + 1024 + width * height];
            byte[] temp_array;
            image_flow[0] = (byte)'B';
            image_flow[1] = (byte)'M';
            temp_array = BitConverter.GetBytes(image_flow.Length);
            Buffer.BlockCopy(temp_array, 0, image_flow, 2, 4);//file length
            temp_array = new byte[4] { 0, 0, 0, 0 };
            Buffer.BlockCopy(temp_array, 0, image_flow, 6, 4);//reserve
            temp_array = BitConverter.GetBytes(1024 + 14 + 40);
            Buffer.BlockCopy(temp_array, 0, image_flow, 10, 4);//offset


            temp_array = BitConverter.GetBytes(40);//
            Buffer.BlockCopy(temp_array, 0, image_flow, 14, 4);//info size
            temp_array = BitConverter.GetBytes(width);
            Buffer.BlockCopy(temp_array, 0, image_flow, 18, 4);//width
            temp_array = BitConverter.GetBytes(-1 * height);
            Buffer.BlockCopy(temp_array, 0, image_flow, 22, 4);//height
            temp_array = new byte[2] { 0, 1 };
            Buffer.BlockCopy(temp_array, 0, image_flow, 26, 2);//planes
            temp_array = BitConverter.GetBytes(8);//fibitcount
            Buffer.BlockCopy(temp_array, 0, image_flow, 28, 2);
            temp_array = new byte[4] { 0, 0, 0, 0 };
            Buffer.BlockCopy(temp_array, 0, image_flow, 30, 4);//compression
            temp_array = new byte[4] { 0, 0, 0, 0 };
            Buffer.BlockCopy(temp_array, 0, image_flow, 34, 4);//imagesize
            temp_array = new byte[8] { 0, 0, 0, 0, 0, 0, 0, 0 };
            Buffer.BlockCopy(temp_array, 0, image_flow, 38, 8);//pixelspermeter
            temp_array = BitConverter.GetBytes(256);
            Buffer.BlockCopy(temp_array, 0, image_flow, 46, 4);//clrused
            temp_array = BitConverter.GetBytes(256);
            Buffer.BlockCopy(temp_array, 0, image_flow, 50, 4);//clrImportant

            temp_array = new byte[1024];
            for (i = 0; i < 256; i++)
            {
                temp_array[i * 4] = (byte)i;
                temp_array[(i * 4) + 1] = (byte)i;
                temp_array[(i * 4) + 2] = (byte)i;
                temp_array[(i * 4) + 3] = 0xff;
            }
            Buffer.BlockCopy(temp_array, 0, image_flow, 54, 1024);//pallate
            Buffer.BlockCopy(image_array, 0, image_flow, 1078, width * height);//image data
            //MemoryStream ms = new MemoryStream(image_flow);
            //Image image = System.Drawing.Image.FromStream(ms);
            return image_flow;


        }


        public static Image get_image_from_byte(byte[] image_array, int width, int height)
        {
            Bitmap image = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
            BitmapData image_data = image.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
            int stride = image_data.Stride;
            int offset = stride - width;
            IntPtr dist_ptr = image_data.Scan0;
            int total_bytes = stride * height;
            int src_count = 0, dist_count = 0;
            byte[] pixelvalues = new byte[total_bytes];
            //Console.WriteLine(image_data.Stride);
            //Console.WriteLine(image_data.Width);
            //Console.WriteLine(image_data.Height);
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    pixelvalues[dist_count++] = image_array[src_count++];
                }
                dist_count += offset;
            }
            System.Runtime.InteropServices.Marshal.Copy(pixelvalues, 0, dist_ptr, total_bytes);
            image.UnlockBits(image_data);
            ColorPalette tempPalette;
            using (Bitmap tempBmp = new Bitmap(1, 1, PixelFormat.Format8bppIndexed))
            {
                tempPalette = tempBmp.Palette;
            }
            for (int i = 0; i < 256; i++)
            {
                tempPalette.Entries[i] = Color.FromArgb(i, i, i);
            }
            image.Palette = tempPalette;
            return image;
        }

        public delegate void myinvoke6();
        public static int flag_pgb7 = 0;
        public static void LoadSensorUtilityBinToMcu()
        {
            int i = 0;
            result_after_issue_cmd = 0x6969;
            if ((Program.mainform.textBox4.InvokeRequired) || (Program.mainform.label19.InvokeRequired))
            {
                myinvoke6 mi = new myinvoke6(LoadSensorUtilityBinToMcu);
                Program.mainform.Invoke(mi);
            }
            else
            {
                flag_pgb7 = 1;
                Thread si = new Thread(new ThreadStart(programSensorUtilityBinToMcu));
                si.IsBackground = true;
                si.Start();

                for (i = 0; i < 1000; i++)
                {
                    if (1 == pgb_changed)
                    {
                        pgb_changed = 0;
                        i = 0;
                    }
                    if (1 != flag_pgb7) break;

                    Thread.Sleep(10);
                    try
                    {
                        Application.DoEvents();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("i am here");
                    }


                }


            }
        }

        public static void programSensorUtilityBinToMcu()
        {
            result_after_issue_cmd = 0x6969;
            if (!(File.Exists(sensor_uti_fw_filename)))//eclipse_secure_matcher_debug.bin
            {
                PS_to_DBB("No sensor uti FW file found\r\n");
                flag_pgb7 = 0;
                return;
            }
            int i = 0, j = 0;
            int file_lenth = 0;
            FileStream fs = new FileStream(sensor_uti_fw_filename, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            file_lenth = (int)fs.Length;
            byte[] binData = new byte[file_lenth];
            binData = br.ReadBytes(file_lenth);
            fs.Close();
            //Console.WriteLine(binData.Length);
            //Console.WriteLine(binData[0]);
            //Console.WriteLine(binData[1]);
            byte block_size = 0x80;
            int nblock = file_lenth / block_size;
            byte lastblock = (byte)(file_lenth % block_size);
            uint offset = 0;
            byte[] apdu_test;
            uint apdu_length = 0;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte LL_byte = (byte)(file_lenth & 0xff);
            byte ML_byte = (byte)((file_lenth >> 8) & 0xff);
            byte MH_byte = (byte)((file_lenth >> 16) & 0xff);
            byte HH_byte = (byte)((file_lenth >> 24) & 0xff);
            //first apdu includes total file length

            uint crc_data = get_file_crc32(sensor_uti_fw_filename);
            byte crc_LL_byte = (byte)(crc_data & 0xff);
            byte crc_ML_byte = (byte)((crc_data >> 8) & 0xff);
            byte crc_MH_byte = (byte)((crc_data >> 16) & 0xff);
            byte crc_HH_byte = (byte)((crc_data >> 24) & 0xff);

            result_after_issue_cmd = connect_the_card();
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb7 = 0;
                return;
            }
            PS_to_DBB("Connect card\r\n");
            //update start
            apdu_test = update_start;
            apdu_length = (uint)update_start.Length;
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd=check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb7 = 0;
                return;
            }
            PS_to_DBB("Load sensor uti FW start done\r\n");
            System.Threading.Thread.Sleep(1000);

            //update details
            //ui
            int percent_value = 0;
            Program.mainform.update_any_pgb(7, percent_value);
            //ui
            List<byte> temp_array_list = new List<byte>(update_details);
            temp_array_list.Add(0x29);//add size
            temp_array_list.Add(0x00);//add address
            temp_array_list.Add(0x80);
            temp_array_list.Add(0x03);
            temp_array_list.Add(0x00);
            temp_array_list.Add(LL_byte);//add file size
            temp_array_list.Add(ML_byte);
            temp_array_list.Add(MH_byte);
            temp_array_list.Add(HH_byte);
            temp_array_list.Add(0x02);//add flag,f[0] = encrypt, f[2:1] = { MCU, Sensor RAM, Sensor OTP, Reserved} 
            temp_array_list.Add(crc_LL_byte);//add crc
            temp_array_list.Add(crc_ML_byte);
            temp_array_list.Add(crc_MH_byte);
            temp_array_list.Add(crc_HH_byte);
            for (i = 0; i < 28; i++)//add IV
            {
                temp_array_list.Add(0x00);
            }
            apdu_test = temp_array_list.ToArray();
            apdu_length = (uint)apdu_test.Length;
            //Console.WriteLine(apdu_length);
            string temp_string = byte_to_hexstr(apdu_test, (int)apdu_length);
            //Console.WriteLine(temp_string+"\r\n");
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb7 = 0;
                return;
            }
            PS_to_DBB("Load sensor uti FW details done\r\n");
            System.Threading.Thread.Sleep(1000);
            //update bin files
            apdu_test = new byte[264];
            for (i = 0; i < update_data.Length; i++)
            {
                apdu_test[i] = update_data[i];
            }

            apdu_test[update_data.Length] = block_size;
            byte temp_offset = (byte)(update_data.Length + 1);

            apdu_length = (byte)(block_size + update_data.Length + 1);
            for (i = 0; i < nblock; i++)
            {
                for (j = 0; j < block_size; j++)
                {
                    apdu_test[temp_offset + j] = binData[offset + j];
                }
                offset += block_size;
                //ui
                uint temp_data = (uint)((i * 100) / (nblock + 1));
                if (percent_value < temp_data)
                {
                    percent_value = (int)temp_data;
                    pgb_changed = 1;
                    Program.mainform.update_any_pgb(7, percent_value);
                }

                //ui
                receive_test_length = 264;
                result_after_issue_cmd = 0x6969;
                result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
                result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
                if (0x9000 != result_after_issue_cmd)
                {
                    flag_pgb7 = 0;
                    return;
                }
            }


            //send last
            apdu_test[update_data.Length] = lastblock;
            apdu_length = (byte)(lastblock + update_data.Length + 1);
            for (j = 0; j < lastblock; j++)
            {
                apdu_test[temp_offset + j] = binData[offset + j];
            }
            offset += block_size;
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb7 = 0;
                return;
            }
            PS_to_DBB("Load sensor uti FW bin done\r\n");
            //send update hash mcu_fw_hash
            temp_array_list = new List<byte>(update_hash);
            temp_array_list.Add(16);
            for (i = 0; i < 16; i++)
            {
                temp_array_list.Add(sensor_uti_fw_hash[i]);
            }
            apdu_test = temp_array_list.ToArray();
            apdu_length = (byte)temp_array_list.Count;
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb7 = 0;
                return;
            }
            PS_to_DBB("Load sensor uti FW hash done\r\n");
            //System.Threading.Thread.Sleep(50);

            //ui
            percent_value = 100;
            Program.mainform.update_any_pgb(7, percent_value);
            //ui

            PS_to_DBB("Load sensor uti FW done\r\n");
            //System.Threading.Thread.Sleep(50);

            flag_pgb7 = 0;
            return;

        }



        public static int flag_pgb6 = 0;
        public static void LoadSensorPrimaryBinToMcu()
        {
            int i = 0;
            result_after_issue_cmd = 0x6969;
            if ((Program.mainform.textBox4.InvokeRequired) || (Program.mainform.label19.InvokeRequired))
            {
                myinvoke6 mi = new myinvoke6(LoadSensorPrimaryBinToMcu);
                Program.mainform.Invoke(mi);
            }
            else
            {
                flag_pgb6 = 1;
                Thread si = new Thread(new ThreadStart(ProgramSensorPrimaryBinToMcu));
                si.IsBackground = true;
                si.Start();

                for (i = 0; i < 1000; i++)
                {
                    if (1 == pgb_changed)
                    {
                        pgb_changed = 0;
                        i = 0;
                    }
                    if (1 != flag_pgb6) break;

                    Thread.Sleep(10);
                    try
                    {
                        Application.DoEvents();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("i am here");
                    }


                }


            }
        }



        public static void ProgramSensorPrimaryBinToMcu()
        {
            result_after_issue_cmd = 0x6969;
            if (!(File.Exists(sensor_pri_fw_filename)))//eclipse_secure_matcher_debug.bin
            {
                PS_to_DBB("No sensor pri FW file found\r\n");
                flag_pgb6 = 0;
                return;
            }
            int i = 0, j = 0;
            int file_lenth = 0;
            FileStream fs = new FileStream(sensor_pri_fw_filename, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            file_lenth = (int)fs.Length;
            byte[] binData = new byte[file_lenth];
            binData = br.ReadBytes(file_lenth);
            fs.Close();
            //Console.WriteLine(binData.Length);
            //Console.WriteLine(binData[0]);
            //Console.WriteLine(binData[1]);
            byte block_size = 0x80;
            int nblock = file_lenth / block_size;
            byte lastblock = (byte)(file_lenth % block_size);
            uint offset = 0;
            byte[] apdu_test;
            uint apdu_length = 0;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte LL_byte = (byte)(file_lenth & 0xff);
            byte ML_byte = (byte)((file_lenth >> 8) & 0xff);
            byte MH_byte = (byte)((file_lenth >> 16) & 0xff);
            byte HH_byte = (byte)((file_lenth >> 24) & 0xff);
            //first apdu includes total file length

            uint crc_data = get_file_crc32(sensor_pri_fw_filename);
            byte crc_LL_byte = (byte)(crc_data & 0xff);
            byte crc_ML_byte = (byte)((crc_data >> 8) & 0xff);
            byte crc_MH_byte = (byte)((crc_data >> 16) & 0xff);
            byte crc_HH_byte = (byte)((crc_data >> 24) & 0xff);
            System.Threading.Thread.Sleep(1000);
            result_after_issue_cmd = connect_the_card();
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb6 = 0;
                return;
            }
            PS_to_DBB("Connect card\r\n");
            System.Threading.Thread.Sleep(1000);

            //update details
            //ui
            int percent_value = 0;
            
            Program.mainform.update_any_pgb(6, percent_value);
            //ui
            List<byte> temp_array_list = new List<byte>(update_details);
            temp_array_list.Add(0x29);//add size
            temp_array_list.Add(0x00);//add address
            temp_array_list.Add(0x00);
            temp_array_list.Add(0x00);
            temp_array_list.Add(0x00);
            temp_array_list.Add(LL_byte);//add file size
            temp_array_list.Add(ML_byte);
            temp_array_list.Add(MH_byte);
            temp_array_list.Add(HH_byte);
            temp_array_list.Add(0x04);//add flag,f[0] = encrypt, f[2:1] = { MCU, Sensor RAM, Sensor OTP, Reserved} 
            temp_array_list.Add(crc_LL_byte);//add crc
            temp_array_list.Add(crc_ML_byte);
            temp_array_list.Add(crc_MH_byte);
            temp_array_list.Add(crc_HH_byte);
            for (i = 0; i < 28; i++)//add IV
            {
                temp_array_list.Add(0x00);
            }
            apdu_test = temp_array_list.ToArray();
            apdu_length = (uint)apdu_test.Length;
            //Console.WriteLine(apdu_length);
            string temp_string = byte_to_hexstr(apdu_test, (int)apdu_length);
            //Console.WriteLine(temp_string+"\r\n");
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb6 = 0;
                return;
            }
            PS_to_DBB("Load sensor Pri FW details done\r\n");
            System.Threading.Thread.Sleep(1000);
            //update bin files
            apdu_test = new byte[264];
            for (i = 0; i < update_data.Length; i++)
            {
                apdu_test[i] = update_data[i];
            }

            apdu_test[update_data.Length] = block_size;
            byte temp_offset = (byte)(update_data.Length + 1);

            apdu_length = (byte)(block_size + update_data.Length + 1);
            for (i = 0; i < nblock; i++)
            {
                for (j = 0; j < block_size; j++)
                {
                    apdu_test[temp_offset + j] = binData[offset + j];
                }
                offset += block_size;
                //ui
                uint temp_data = (uint)((i * 100) / (nblock + 1));
                if (percent_value < temp_data)
                {
                    percent_value = (int)temp_data;
                    pgb_changed = 1;
                    Program.mainform.update_any_pgb(6, percent_value);
                }

                //ui
                receive_test_length = 264;
                result_after_issue_cmd = 0x6969;
                result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
                result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
                if (0x9000 != result_after_issue_cmd)
                {
                    flag_pgb6 = 0;
                    return;
                }
            }


            //send last
            apdu_test[update_data.Length] = lastblock;
            apdu_length = (byte)(lastblock + update_data.Length + 1);
            for (j = 0; j < lastblock; j++)
            {
                apdu_test[temp_offset + j] = binData[offset + j];
            }
            offset += block_size;
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb6 = 0;
                return;
            }
            PS_to_DBB("Load sensor pri FW bin done\r\n");
            //send update hash mcu_fw_hash
            temp_array_list = new List<byte>(update_hash);
            temp_array_list.Add(16);
            for (i = 0; i < 16; i++)
            {
                temp_array_list.Add(sensor_pri_fw_hash[i]);
            }
            apdu_test = temp_array_list.ToArray();
            apdu_length = (byte)temp_array_list.Count;
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb6 = 0;
                return;
            }
            PS_to_DBB("Load sensor pri FW hash done\r\n");
            System.Threading.Thread.Sleep(1000);
            //update end   
            apdu_test = update_end;
            apdu_length = (byte)update_end.Length;
            receive_test_length = 264;
            //ui
            percent_value = 100;
            Program.mainform.update_any_pgb(6, percent_value);
            //ui
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb6 = 0;
                return;
            }
            PS_to_DBB("Load sensor priFW end done\r\n");
            PS_to_DBB("Load sensor pri FW done\r\n");
            //System.Threading.Thread.Sleep(50);


            return;

        }

        public static void PS_to_DBB(string ps)
        {
            Program.mainform.print_string_to_debugbox(ps);
        }

        public static int flag_pgb5 = 0;

        public static void LoadMcuFWBinToMcu()
        {
            int i = 0;
            result_after_issue_cmd = 0x6969;
            if ((Program.mainform.textBox4.InvokeRequired) || (Program.mainform.label19.InvokeRequired))
            {
                myinvoke6 mi = new myinvoke6(LoadMcuFWBinToMcu);
                Program.mainform.Invoke(mi);
            }
            else
            {
                flag_pgb5 = 1;
                Thread si = new Thread(new ThreadStart(programMcuFWBinToMcu));
                si.IsBackground = true;
                si.Start();

                for (i = 0; i < 2000; i++)
                {
                    if (1 == pgb_changed)
                    {
                        pgb_changed = 0;
                        i = 0;
                    }
                    if (1 != flag_pgb5) break;

                    Thread.Sleep(10);
                    try
                    {
                        Application.DoEvents();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("i am here");
                    }


                }


            }
        }

        public static void programMcuFWBinToMcu()
        {
            result_after_issue_cmd = 0x6969;
            if (!(File.Exists(mcu_fw_filename)))//eclipse_secure_matcher_debug.bin
            {
                PS_to_DBB("No MCU FW file found\r\n");
                flag_pgb5 = 0;
                return;
            }
            int i = 0, j = 0;
            int file_lenth = 0;
            FileStream fs = new FileStream(mcu_fw_filename, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            file_lenth = (int)fs.Length;
            byte[] binData = new byte[file_lenth];
            binData = br.ReadBytes(file_lenth);
            fs.Close();
            //Console.WriteLine(binData.Length);
            //Console.WriteLine(binData[0]);
            //Console.WriteLine(binData[1]);
            byte block_size = 0xe0;
            int nblock = file_lenth / block_size;
            byte lastblock = (byte)(file_lenth % block_size);
            uint offset = 0;
            byte[] apdu_test;
            uint apdu_length = 0;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte LL_byte = (byte)(file_lenth & 0xff);
            byte ML_byte = (byte)((file_lenth >> 8) & 0xff);
            byte MH_byte = (byte)((file_lenth >> 16) & 0xff);
            byte HH_byte = (byte)((file_lenth >> 24) & 0xff);
            //first apdu includes total file length

            System.Threading.Thread.Sleep(3000);
            result_after_issue_cmd = connect_the_card();
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb5 = 0;
                return;
            }
            PS_to_DBB("Connect card\r\n");
            System.Threading.Thread.Sleep(1000);
            //update start
            apdu_test = update_start;
            apdu_length = (uint)update_start.Length;
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb5 = 0;
                return;
            }
            PS_to_DBB("Load MCU FW start done\r\n");
            System.Threading.Thread.Sleep(1000);
            //update details
            //ui
            int percent_value = 0;
            
            Program.mainform.update_any_pgb(5, percent_value);
            //ui
            List<byte> temp_array_list = new List<byte>(update_details);
            temp_array_list.Add(0x29);//add size
            temp_array_list.Add(0x00);//add address
            temp_array_list.Add(0x00);
            temp_array_list.Add(0x00);
            temp_array_list.Add(0x08);
            temp_array_list.Add(LL_byte);//add file size
            temp_array_list.Add(ML_byte);
            temp_array_list.Add(MH_byte);
            temp_array_list.Add(HH_byte);
            temp_array_list.Add(0x00);//add flag,f[0] = encrypt, f[2:1] = { MCU, Sensor RAM, Sensor OTP, Reserved} 
            for (i = 0; i < 32; i++)//add IV
            {
                temp_array_list.Add(0x00);
            }
            apdu_test = temp_array_list.ToArray();
            apdu_length = (uint)apdu_test.Length;
            //Console.WriteLine(apdu_length);
            string temp_string = byte_to_hexstr(apdu_test, (int)apdu_length);
            //Console.WriteLine(temp_string+"\r\n");
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb5 = 0;
                return;
            }
            PS_to_DBB("Load MCU FW details done\r\n");
            System.Threading.Thread.Sleep(1000);
            //update bin files
            apdu_test = new byte[264];
            for (i = 0; i < update_data.Length; i++)
            {
                apdu_test[i] = update_data[i];
            }

            apdu_test[update_data.Length] = block_size;
            byte temp_offset = (byte)(update_data.Length + 1);

            apdu_length = (byte)(block_size + update_data.Length + 1);
            for (i = 0; i < nblock; i++)
            {
                for (j = 0; j < block_size; j++)
                {
                    apdu_test[temp_offset + j] = binData[offset + j];
                }
                offset += block_size;
                //ui
                uint temp_data = (uint)((i * 100) / (nblock + 1));
                if (percent_value < temp_data)
                {
                    percent_value = (int)temp_data;
                    pgb_changed = 1;
                    Program.mainform.update_any_pgb(5, percent_value);

                }

                //ui
                receive_test_length = 264;
                result_after_issue_cmd = 0x6969;
                result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
                result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
                if (0x9000 != result_after_issue_cmd)
                {
                    flag_pgb5 = 0;
                    return;
                }
            }


            //send last
            apdu_test[update_data.Length] = lastblock;
            apdu_length = (byte)(lastblock + update_data.Length + 1);
            for (j = 0; j < lastblock; j++)
            {
                apdu_test[temp_offset + j] = binData[offset + j];
            }
            offset += block_size;
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb5 = 0;
                return;
            }
            PS_to_DBB("Load MCU FW bin done\r\n");
            //send update hash mcu_fw_hash
            temp_array_list = new List<byte>(update_hash);
            temp_array_list.Add(16);
            for (i = 0; i < 16; i++)
            {
                temp_array_list.Add(mcu_fw_hash[i]);
            }
            apdu_test = temp_array_list.ToArray();
            apdu_length = (byte)temp_array_list.Count;
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb5 = 0;
                return;
            }
            PS_to_DBB("Load MCU FW hash done\r\n");
            //System.Threading.Thread.Sleep(50);
            //update end   
            apdu_test = update_end;
            apdu_length = (byte)update_end.Length;
            receive_test_length = 264;
            //ui
            percent_value = 100;
            Program.mainform.update_any_pgb(5, percent_value);
            //ui
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb5 = 0;
                return;
            }
            PS_to_DBB("Load MCU FW end done\r\n");
            PS_to_DBB("Load MCU FW done\r\n");
            //System.Threading.Thread.Sleep(50);

            flag_pgb5 = 0;
            return;

        }





        public static int flag_pgb4 = 0;

        public static void LoadUpdateBinToMcu()
        {
            int i = 0;
            result_after_issue_cmd = 0x6969;
            if ((Program.mainform.textBox4.InvokeRequired) || (Program.mainform.label19.InvokeRequired))
            {
                myinvoke6 mi = new myinvoke6(LoadUpdateBinToMcu);
                Program.mainform.Invoke(mi);
            }
            else
            {
                flag_pgb4 = 1;
                Thread si = new Thread(new ThreadStart(programUpdateBinToMcu));
                si.IsBackground = true;
                si.Start();

                for (i = 0; i < 1000; i++)
                {
                    if (1 == pgb_changed)
                    {
                        pgb_changed = 0;
                        i = 0;
                    }
                    if (1 != flag_pgb4) break;

                    Thread.Sleep(10);
                    try
                    {
                        Application.DoEvents();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("i am here");
                    }


                }

            }
        }



        public static void programUpdateBinToMcu()
        {
            result_after_issue_cmd = 0x6969;
            if (!(File.Exists(update_bin_filename)))//eclipse_secure_matcher_debug.bin
            {
                PS_to_DBB("No Update.bin file found\r\n");
                flag_pgb4 = 0;
                return;
            }
            int i = 0, j = 0;
            int file_lenth = 0;
            FileStream fs = new FileStream(update_bin_filename, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            file_lenth = (int)fs.Length;
            byte[] binData = new byte[file_lenth];
            binData = br.ReadBytes(file_lenth);
            fs.Close();
            //Console.WriteLine(binData.Length);
            //Console.WriteLine(binData[0]);
            //Console.WriteLine(binData[1]);
            byte block_size = 0xe0;
            int nblock = file_lenth / block_size;
            byte lastblock = (byte)(file_lenth % block_size);
            uint offset = 0;
            byte[] apdu_test = new byte[264];
            uint apdu_length = 0;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            byte low_byte = (byte)(file_lenth & 0xff);
            byte high_byte = (byte)((file_lenth >> 8) & 0xff);
            //first apdu includes total file length
            result_after_issue_cmd = connect_the_card();
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb4 = 0;
                return;
            }
            //send first apdu
            //ui
            int percent_value = 0;

            
            Program.mainform.update_any_pgb(4, percent_value);
            //ui
            apdu_test[0] = store_blob[0];
            apdu_test[1] = store_blob[1];
            apdu_test[2] = store_blob[2];
            apdu_test[3] = 0x40;
            apdu_test[4] = (byte)(block_size + 5);
            apdu_test[5] = 0x01;
            apdu_test[6] = low_byte;
            apdu_test[7] = high_byte;
            apdu_test[8] = 0x01;
            apdu_test[9] = 0x00;
            apdu_length = (byte)(block_size + 10);
            for (j = 0; j < block_size; j++)
            {
                apdu_test[10 + j] = binData[offset + j];
            }
            offset += block_size;

            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb4 = 0;
                return;
            }
            //send middle
            apdu_test[4] = block_size;
            apdu_length = (byte)(block_size + 5);
            for (i = 1; i < nblock; i++)
            {

                for (j = 0; j < block_size; j++)
                {
                    apdu_test[5 + j] = binData[offset + j];
                }
                offset += block_size;
                //ui
                uint temp_data = (uint)((i * 100) / (nblock + 1));
                if (percent_value < temp_data)
                {
                    percent_value = (int)temp_data;
                    pgb_changed = 1;
                    Program.mainform.update_any_pgb(4, percent_value);
                }

                //ui
                receive_test_length = 264;
                result_after_issue_cmd = 0x6969;
                result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
                result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
                if (0x9000 != result_after_issue_cmd)
                {
                    flag_pgb4 = 0;
                    return;
                }

            }


            //send last
            apdu_test[3] = 0xc0;
            apdu_test[4] = lastblock;
            apdu_length = (byte)(lastblock + 5);
            for (j = 0; j < lastblock; j++)
            {
                apdu_test[5 + j] = binData[offset + j];
            }
            offset += block_size;
            //ui
            percent_value = 100;
            Program.mainform.update_any_pgb(4, percent_value);
            //ui
            receive_test_length = 264;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb4 = 0;
                return;
            }


            PS_to_DBB("Load update.bin done\r\n");

            result_after_issue_cmd = Disconnect_the_card((int)Disposition_mode.SCARD_UNPOWER_CARD);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb4 = 0;
                return;
            }
            PS_to_DBB("power reset\r\n");
            flag_pgb4 = 0;
            return;
        }



        public static int flag_pgb2 = 0;

        public static void FM_update_SE_FW()
        {
            FM1280_helper.FM_update_SE_FW();
        }

        public static void update_SE_FW()
        {
            switch (SE_category)
            {
                case 1:
                    break;
                case 0:
                default:
                    {
                        THD89_helper.thd89_update_SE_FW();
                    }
                    break;
            }
        }


        public static int flag_pgbl = 0;

        public static void return_to_bl_mode()
        {
            switch (SE_category)
            {
                case 1:
                    break;
                case 0:
                default:
                    {
                        THD89_helper.THD89_return_to_bl_mode();
                    }
                    break;
            }
        }

        public static uint delete_database()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = delete_record;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }





        public static uint FM1280_return_app()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = FM1280_app_apdu;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }






        public static uint open_scp03()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];



            byte[] apdu_test = new byte[] { 0x00, 0x54, 0xd2, 0x00, 0x00 };
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            if (0x9000 != ret_data) return ret_data;

            apdu_test = new byte[] { 0x00, 0x54, 0xd3, 0x00, 0x00 };
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            if (0x9000 != ret_data) return ret_data;

            apdu_test = new byte[] { 0x00, 0x54, 0xd1, 0x00, 0x00 };
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);

            return ret_data;

        }



        public static uint open_scp03_v256()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];



            byte[] apdu_test = new byte[] { 0x00, 0x54, 0xdd, 0x00, 0x00 };
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            return ret_data;

        }

        public static uint set_factory_key_v256()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];

            byte[] apdu_test = new byte[] { 0x00, 0x54, 0xe0, 0x00, 0x00 };
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            if (0x9000 != ret_data) return ret_data;
            return ret_data;
        }

        public static uint set_factory_key()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];



            byte[] apdu_test = new byte[] { 0x00, 0x54, 0xe0, 0x00, 0x00};
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            if (0x9000 != ret_data) return ret_data;

            apdu_test = new byte[] { 0x00, 0x54, 0xd0, 0x00, 0x00 };
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            return ret_data;
            
        }

        public static uint reset_secure_key()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];



            byte[] apdu_test = new byte[] { 0x00, 0x54, 0x24, 0x08, 0x01, 0x00 };
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            if (0x9000 != ret_data) return ret_data;

            apdu_test = new byte[] { 0x00, 0x54, 0x24, 0x08, 0x01, 0x01 };
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            if (0x9000 != ret_data) return ret_data;



            apdu_test = Reset_key;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data= check_ret_data(ret_data);

            return ret_data;
        }


        public static uint image_capture(byte mode)
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            List<byte> temp_array_list = new List<byte>(prepare_image);
            temp_array_list.Add(mode);
            byte[] apdu_test = temp_array_list.ToArray();
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }

        public static uint Force_cal_and_store(byte option)
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            List<byte> temp_array_list = new List<byte>(calibrate);
            temp_array_list.Add(option);
            byte[] apdu_test = temp_array_list.ToArray();
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }


        public delegate void myinvoke3();
        public static void Send_customize_apdu()
        {
            if (Program.mainform.textBox18.InvokeRequired)
            {
                myinvoke3 mi = new myinvoke3(Send_customize_apdu);
                Program.mainform.Invoke(mi);
            }
            else
            {
                go_customize_apdu();
            }
        }

        public static void go_customize_apdu()
        {
            string apdu_text = Program.mainform.textBox18.Text;
            result_after_issue_cmd = 0x6969;
            if (apdu_text == "") return;
            string temp_string = apdu_text.Replace("\n", "").Replace(" ", "").Replace("\t", "").Replace("\r", "");
            PS_to_DBB(temp_string + "\r\n");

            if (temp_string.Length == 0)
            {
                PS_to_DBB("APDU:" + temp_string + " is not supported\r\n");
                return;
            }
            if (1 == (temp_string.Length % 2))
            {
                temp_string += "0";
            }
            uint apdu_length = (uint)(temp_string.Length / 2);
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = new byte[apdu_length];
            for (int i = 0; i < apdu_length; i++)
            {
                apdu_test[i] = Convert.ToByte(temp_string.Substring(i * 2, 2), 16);
            }
            result_after_issue_cmd = connect_the_card();
            if (0x9000 != result_after_issue_cmd) return;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            //Console.WriteLine(result_after_issue_cmd);
            string response_string = byte_to_hexstr(receive_test, (int)receive_test_length);
            PS_to_DBB("Response:" + response_string + "\r\n");
            //Console.WriteLine(result_after_issue_cmd);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            return;
        }



        public static uint verify_load_image()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = SE_EMK_secure_match;//SE_EMK_verify_load_image;//SE_EMK_secure_match
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);

            string temp_string = byte_to_hexstr(receive_test, (int)receive_test_length);
            PS_to_DBB("Response:" + temp_string + "\r\n");
            if (2 > receive_test_length)
            {
                PS_to_DBB("Response error\r\n");
                return ret_data;
            }
            ret_data = check_ret_data((uint)(receive_test[2]+(receive_test[3]*256)));
            if (0x6300 == ret_data)
            {
                PS_to_DBB("Result: Unmatched\r\n");
                return ret_data;
            }
            if (0x9000 != ret_data)
            {

                PS_to_DBB("Result: Verify error\r\n");
                return ret_data;
            }
            byte finger = receive_test[7];
            byte template = receive_test[6];
            byte result = receive_test[receive_test_length - 3];
            if (0x2f != result)
            {
                PS_to_DBB("Result: Unmatched\r\n");
                return ret_data;

            }
            string signature = temp_string.Substring(80, 14);
            PS_to_DBB("Result: Matched\r\n");
            PS_to_DBB("Finger:");
            print_data_to_debugbox(finger + 1);
            PS_to_DBB("\r\n");
            PS_to_DBB("Template:");
            print_data_to_debugbox(template + 1);
            PS_to_DBB("\r\n");
            PS_to_DBB("Signature:" + signature + "\r\n");


            return 0x9000;
        }


        public static void load_image_for_verify()
        {
            int i = 0;
            result_after_issue_cmd = 0x6969;
            if ((Program.mainform.groupBox8.InvokeRequired) || (Program.mainform.pictureBox1.InvokeRequired))
            {
                myinvoke6 mi = new myinvoke6(load_image_for_verify);
                Program.mainform.Invoke(mi);
            }
            else
            {
                flag_pgb3 = 1;
                Program.mainform.groupBox8.Text = "Fingerprint image loading...";
                Program.mainform.groupBox8.Update();
                Application.DoEvents();
                Thread si = new Thread(new ThreadStart(load_image_to_card));
                si.IsBackground = true;
                si.Start();

                for (i = 0; i < 1000; i++)
                {
                    if (1 == pgb_changed)
                    {
                        pgb_changed = 0;
                        i = 0;
                    }
                    if (1 != flag_pgb3) break;

                    Thread.Sleep(10);
                    try
                    {
                        Application.DoEvents();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("i am here");
                    }
                }

                Program.mainform.groupBox8.Text = "Fingerprint image";
                Program.mainform.groupBox8.Update();
                Application.DoEvents();
                if (0x9000 == result_after_issue_cmd)
                {
                    PS_to_DBB("Image loading successfully done.\r\n");
                }
            }
        }



        public static void load_image_to_card()
        {
            result_after_issue_cmd = 0x6969;
            byte[] image_array = null;
            uint i = 0;
            uint j = 0;
            uint length = 0;
            byte block_size = 0xe0;
            uint nblock = 0;
            byte last_block = 0;
            uint receive_test_length = 0;
            uint offset = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = new byte[264];
            uint apdu_lenth = 0;
            if (false == File.Exists(load_image_filename))
            {
                PS_to_DBB("File does not exist.\r\n");
                result_after_issue_cmd = 0x6969;
                flag_pgb3 = 0;
                return;
            }
            result_after_issue_cmd = connect_the_card();
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb3 = 0;
                return;
            }
            image_array = get_image_array_from_image(load_image_filename);
            length = (uint)image_array.Length;
            if ((length != 17292) && (length != 16641))
            {
                PS_to_DBB("Image size is not support.\r\n");
                result_after_issue_cmd = 0x6969;
                flag_pgb3 = 0;
                return;
            }
            nblock = length / block_size;
            last_block = (byte)(length % block_size);
            apdu_test[0] = load_image[0];
            apdu_test[1] = load_image[1];
            apdu_test[2] = load_image[2];
            apdu_test[3] = 0x40;
            apdu_test[4] = block_size;
            apdu_lenth = (uint)(block_size + 5);

            //ui
            int percent_value = 0;
            Program.mainform.update_any_pgb(3, percent_value);
            //Application.DoEvents();
            //ui
            for (i = 0; i < nblock; i++)
            {
                for (j = 0; j < block_size; j++)
                {
                    apdu_test[5 + j] = image_array[offset + j];
                }
                offset += block_size;
                //ui
                uint temp_data = (uint)((i * 100) / (nblock + 1));
                if (percent_value < temp_data)
                {
                    percent_value = (int)temp_data;
                    pgb_changed = 1;
                    Program.mainform.update_any_pgb(3, percent_value);
                    //Application.DoEvents();
                }
                //ui
                receive_test_length = 264;
                result_after_issue_cmd = 0x6969;
                result_after_issue_cmd = common_send_apdu(apdu_test, apdu_lenth, ref receive_test, ref receive_test_length);
                result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
                if (0x9000 != result_after_issue_cmd)
                {
                    flag_pgb3 = 0;
                    //Console.WriteLine("222222222222222");
                    //Console.WriteLine(result_after_issue_cmd);
                    return;
                }

            }
            //ui

            //ui
            apdu_test[3] = 0xc0;
            apdu_test[4] = last_block;
            for (j = 0; j < last_block; j++)
            {
                apdu_test[5 + j] = image_array[offset + j];
            }
            //ui
            percent_value = 100;

            Program.mainform.update_any_pgb(3, percent_value);
            //Application.DoEvents();

            //ui
            receive_test_length = 264;
            apdu_lenth = (uint)(last_block + 5);
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, apdu_lenth, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            flag_pgb3 = 0;
            return;
        }

        public static string common_file_name;
        public static int common_image_width = 0;
        public static int common_image_height = 0;

        public static Image finger_image_to_save;
        public static string finger_image_name_to_save;
        public static string finger_image_TXT_name_to_save;
        public static string finger_image_BIN_name_to_save;

        public static string MCU_ID = "";
        public static string SENSOR_ID = "";
        public static string SENSOR_M_ID = "";

        public static byte[] fingerprint_image;


        public delegate void myinvoke4(byte mode);
        public static void image_to_pc(byte mode)
        {
            result_after_issue_cmd = 0x6969;
            if ((Program.mainform.groupBox8.InvokeRequired) || (Program.mainform.pictureBox1.InvokeRequired))
            {
                myinvoke4 mi = new myinvoke4(image_to_pc);
                Program.mainform.Invoke(mi, new object[] { mode });
            }
            else
            {
                result_after_issue_cmd = connect_the_card();
                if (0x9000 != result_after_issue_cmd) return;
                if (0 == Initialize_status_value)
                {
                    Initialize_status_value = 2;
                    result_after_issue_cmd = 0x6969;
                    result_after_issue_cmd = common_cd_ini_test(2);
                    if (0x9000 != result_after_issue_cmd) return;
                }
                if (0x01 == mode)
                {
                    PS_to_DBB("Empty image will captured, please wait...\r\n");
                }
                else
                {
                    PS_to_DBB("Place your finger\r\n");
                }
                result_after_issue_cmd = 0x6969;
                result_after_issue_cmd = image_capture(mode);
                if (0x9000 != result_after_issue_cmd) return;
                transfer_image_to_pc();
            }
        }
        public static void transfer_image_to_pc()
        {
            result_after_issue_cmd = 0x6969;
            int i = 0;
            //ui
            Program.mainform.groupBox8.Text = "Fingerprint image reading...";
            Program.mainform.groupBox8.Update();
            //ui
            //flag_pgb3 = 1;
            //show_image();
            //result_after_issue_cmd = show_image();

            flag_pgb3 = 1;
            Thread si = new Thread(new ThreadStart(show_image));
            si.IsBackground = true;
            si.Start();

            for (i = 0; i < 1000; i++)
            {
                if (1 == pgb_changed)
                {
                    pgb_changed = 0;
                    i = 0;
                }
                if (1 != flag_pgb3) break;

                Thread.Sleep(10);
                try
                {
                    Application.DoEvents();
                }
                catch (Exception)
                {
                    Console.WriteLine("i am here");
                }
            }


            //ui
            Program.mainform.groupBox8.Text = "Fingerprint image";

            Program.mainform.pictureBox1.Image = finger_image_to_save;
            Application.DoEvents();
            //ui
            if (0x9000 != result_after_issue_cmd)
            {
                PS_to_DBB("Image display failed\r\n");
                return;
            }
            create_image_file_and_save();
            PS_to_DBB("Image display done\r\n");
            return;
        }


        public static void show_image()
        {
            int percent_value = 0;

            current_percent_value = 0;
            old_percent_value = 0;
            //uint ret_data = 0x6969;

            uint receive_test_length = 0;
            byte block_size = 128;
            uint nblock = 0;
            byte last_block = 0;
            uint image_height = 0;
            uint image_width = 0;
            uint image_length = 0;

            uint fingerprint_image_size = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = get_image;
            result_after_issue_cmd = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            if (0x9000 != result_after_issue_cmd)
            {
                flag_pgb3 = 0;
                return;
            }
            string temp_string = byte_to_hexstr(receive_test, (int)receive_test_length);
            //print_string_to_debugbox(temp_string+"\r\n");
            fingerprint_image_size = (uint)receive_test[7] * 256 + receive_test[6];
            fingerprint_image_size = get_response_data_length(fingerprint_image_size);
            image_length = fingerprint_image_size;
            if (17292 == image_length)
            {
                image_width = 132;
                image_height = 131;
            }
            if (16641 == image_length)
            {
                image_width = 129;
                image_height = 129;
            }
            if (0 == image_height)
            {
                PS_to_DBB("Image is empty or not supported\r\n");
                result_after_issue_cmd = 0x6969;
                flag_pgb3 = 0;
                return;
            }
            fingerprint_image = new byte[image_length];
            nblock = image_length / block_size;
            last_block = (byte)(image_length % block_size);
            //print_data_to_debugbox((int)nblock);
            //print_string_to_debugbox("\r\n");
            //print_data_to_debugbox((int)last_block);
            //print_string_to_debugbox("\r\n");
            List<byte> temp_array_list = new List<byte>(read_data);
            temp_array_list.Add(0x00);
            temp_array_list.Add(0x00);
            temp_array_list.Add(0x00);
            temp_array_list.Add(0x00);
            uint picxl_count = 0;
            byte[] apdu_read = temp_array_list.ToArray();
            byte offset_low = 0;
            byte offset_high = 0;
            uint i = 0;
            uint j = 0;
            uint temp_size = 0;
            Program.mainform.update_any_pgb(3, percent_value);
            //Application.DoEvents();
            for (i = 0; i < nblock + 1; i++)
            {
                offset_low = (byte)((i * block_size) & 0xff);
                offset_high = (byte)(((i * block_size) >> 8) & 0xff);
                //Console.WriteLine(offset_low);
                //Console.WriteLine(offset_high);
                apdu_read[read_data.Length] = offset_low;
                apdu_read[read_data.Length + 1] = offset_high;
                apdu_read[read_data.Length + 2] = block_size;
                if (i == nblock) apdu_read[read_data.Length + 2] = last_block;
                receive_test_length = 264;
                result_after_issue_cmd = 0x6969;
                result_after_issue_cmd = common_send_apdu(apdu_read, (uint)apdu_read.Length, ref receive_test, ref receive_test_length);
                result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
                //Application.DoEvents();
                if (0x9000 != result_after_issue_cmd)
                {
                    flag_pgb3 = 0;
                    return;
                }
                //Console.WriteLine(receive_test_length);
                //temp_string = byte_to_hexstr(receive_test, (int)receive_test_length);
                //print_string_to_debugbox(temp_string + "\r\n");
                //todo creat image array
                temp_size = (uint)(receive_test[1] * 256 + receive_test[0] - 8);
                temp_size=get_response_data_length(temp_size);
                for (j = 0; j < temp_size; j++)
                {
                    fingerprint_image[picxl_count + j] = receive_test[j + 6];
                }
                picxl_count += temp_size;
                //ui
                uint temp_data = (uint)((i * 100) / (nblock + 1));
                if (percent_value < temp_data)
                {
                    percent_value = (int)temp_data;
                    current_percent_value = percent_value;
                    pgb_changed = 1;
                    Program.mainform.update_any_pgb(3, percent_value);
                    //Thread.Sleep(100);
                }
                //ui

            }

            percent_value = 100;
            Program.mainform.update_any_pgb(3, percent_value);
            //Application.DoEvents();
            if (picxl_count != fingerprint_image_size)
            {
                PS_to_DBB("Image read error\r\n");
                result_after_issue_cmd = 0x6969;
                flag_pgb3 = 0;
                return;
            }
            //todo show image
            string current_time = DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss");

            common_file_name = current_time + MCU_ID + "_" + SENSOR_ID;
            common_image_width = (int)image_width;
            common_image_height = (int)image_height;
            Image test_image = get_image_from_byte(fingerprint_image, (int)image_width, (int)image_height);
            finger_image_to_save = test_image;

            result_after_issue_cmd = 0x9000;
            flag_pgb3 = 0;

            return;
        }



        public static void create_image_file_and_save()
        {
            string app_path = System.Environment.CurrentDirectory;
            string image_path;
            image_path = app_path + "\\Img_log";
            if (false == Directory.Exists(image_path + "\\BMP"))
            {
                Directory.CreateDirectory(image_path + "\\BMP");
            }
            if (false == Directory.Exists(image_path + "\\TXT"))
            {
                Directory.CreateDirectory(image_path + "\\TXT");
            }
            if (false == Directory.Exists(image_path + "\\BIN"))
            {
                Directory.CreateDirectory(image_path + "\\BIN");
            }

            finger_image_TXT_name_to_save = image_path + "\\TXT" + "\\" + common_file_name + ".txt";
            finger_image_name_to_save = image_path + "\\BMP" + "\\" + common_file_name + ".bmp";
            finger_image_BIN_name_to_save = image_path + "\\BIN" + "\\" + common_file_name + ".bin";

            switch (Program.mainform.comboBox10.Text)
            {
                case "TXT":
                    {
                        save_image_to_TXT(fingerprint_image, finger_image_TXT_name_to_save, common_image_width, common_image_height);
                    }
                    break;
                case "BMP":
                    {
                        //finger_image_to_save.Save(finger_image_name_to_save);
                        //CreateImageFromBytes(finger_image_name_to_save, fingerprint_image);
                        byte[] temp_image = get_image_from_array(fingerprint_image, finger_image_to_save.Width, finger_image_to_save.Height);
                        save_image_to_BIN(temp_image, finger_image_name_to_save);
                        //temp_image.Save(finger_image_name_to_save);
                    }
                    break;
                case "BIN":
                    {
                        save_image_to_BIN(fingerprint_image, finger_image_BIN_name_to_save);
                    }
                    break;
                default:
                    {
                        save_image_to_TXT(fingerprint_image, finger_image_TXT_name_to_save, common_image_width, common_image_height);
                        finger_image_to_save.Save(finger_image_name_to_save);
                        //CreateImageFromBytes(finger_image_name_to_save, fingerprint_image);
                        save_image_to_BIN(fingerprint_image, finger_image_BIN_name_to_save);
                        //Image temp_image = get_image_from_array(fingerprint_image, finger_image_to_save.Width, finger_image_to_save.Height);
                        //temp_image.Save(finger_image_name_to_save);
                    }
                    break;
            }
            return;
        }

        public static void save_image_to_TXT(byte[] image_array, string txt_name, int image_width, int image_height)
        {
            int i;
            int j;
            FileStream fs = new FileStream(txt_name, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
            string txt_string;
            for (i = 0; i < image_height; i++)
            {
                txt_string = "";
                for (j = 0; j < image_width; j++)
                {
                    txt_string += image_array[i * image_width + j].ToString("X2") + ",";
                }
                sw.WriteLine(txt_string);
            }
            sw.Close();
            fs.Close();
        }
        public static void save_image_to_BIN(byte[] image_array, string txt_name)
        {
            int i;
            BinaryWriter bw = new BinaryWriter(new FileStream(txt_name, FileMode.Create));
            bw.Write(image_array);
            bw.Close();
        }



        public static uint get_all_id()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            List<byte> temp_array_list = new List<byte>(get_UID);
            temp_array_list.Add(0x00);
            byte[] apdu_test = temp_array_list.ToArray();
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;

            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            if (0x9000 != ret_data) return ret_data;
            string temp_string = byte_to_hexstr(receive_test, (int)receive_test_length);
            MCU_ID = temp_string.Substring(12, 24);
            PS_to_DBB("MCU ID:" + MCU_ID + "\r\n");

            apdu_test[apdu_test.Length - 1] = 0x01;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            if (0x9000 != ret_data) return ret_data;
            temp_string = byte_to_hexstr(receive_test, (int)receive_test_length);
            SENSOR_ID = temp_string.Substring(12, 8);
            PS_to_DBB("Sensor ID:" + SENSOR_ID + "\r\n");

            apdu_test[apdu_test.Length - 1] = 0x02;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            if (0x9000 != ret_data) return ret_data;
            temp_string = byte_to_hexstr(receive_test, (int)receive_test_length);
            SENSOR_M_ID = temp_string.Substring(12, 12);
            PS_to_DBB("Sensor M ID:" + SENSOR_M_ID + "\r\n");


            return ret_data;
        }


        public static uint enroll_one_finger(int finger_number)
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            if (0 == Initialize_status_value)
            {
                Initialize_status_value = 2;
                ret_data = common_cd_ini_test(2);
                if (0x9000 != ret_data) return ret_data;
            }
            byte[] apdu_test = SE_EMK_enroll;
            if (2== finger_number) { apdu_test[3]= 0x26; }
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }


        public static uint secure_verify_finger(uint flag)
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];

            List<byte> temp_array_list = new List<byte>(SE_EMK_secure_match);
            string mwf_string = "";
            int mwf_data = 0;
            if ("" == mwf_string)
            { mwf_data = 7902; }
            else
            { 
                try
                {
                    mwf_data = Convert.ToInt32(mwf_string, 10);
                    //Console.WriteLine(mwf_data);
                }
                catch (Exception ex)
                {
                    PS_to_DBB("please input correct mwf value, 4000~10000.\r\n");
                    return 0x6969;
                }
                if ((4000 > mwf_data) || (10000 < mwf_data))
                {
                    PS_to_DBB("please input correct mwf value, 4000~10000.\r\n");
                    return 0x6969;
                }
            }
            byte[] temp_byte = BitConverter.GetBytes(mwf_data);//BitConverter.GetBytes(width);
            temp_array_list.Add(temp_byte[0]);
            temp_array_list.Add(temp_byte[1]);


            byte[] apdu_test = temp_array_list.ToArray();
            if (1==flag) { apdu_test[5]= 1; }
            
            //string tmp_string = byte_to_hexstr(apdu_test, (int)apdu_test.Length);
            //Console.WriteLine(tmp_string);
            //return 0x6969;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);

            string temp_string = byte_to_hexstr(receive_test, (int)receive_test_length);
            PS_to_DBB("Response:" + temp_string + "\r\n");
            if (2 > receive_test_length)
            {
                PS_to_DBB("Response error.\r\n");
                return ret_data;
            }
            ret_data = check_ret_data((uint)(receive_test[2] + (receive_test[3] * 256)));
            if (0x6300 == ret_data)
            {
                PS_to_DBB("Result: Unmatched\r\n");
                return ret_data;
            }
            if (0x9000 != ret_data)
            {

                PS_to_DBB("Result: Verify error\r\n");
                return ret_data;
            }
            PS_to_DBB("Result: Matched\r\n");
            /*
            byte finger = receive_test[7];
            byte template = receive_test[6];
            byte result = receive_test[receive_test_length - 3];
            if (0x2f != result)
            {
                PS_to_DBB("Result: Unmatched\r\n");
                return ret_data;

            }
            string signature = temp_string.Substring(80, 14);
            
            PS_to_DBB("Finger:");
            print_data_to_debugbox(finger + 1);
            PS_to_DBB("\r\n");
            PS_to_DBB("Template:");
            print_data_to_debugbox(template + 1);
            PS_to_DBB("\r\n");
            PS_to_DBB("Signature:" + signature + "\r\n");
            */

            return 0x9000;
        }

        public static uint verify_finger(byte mode)
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            if (0 == Initialize_status_value)
            {
                Initialize_status_value = mode;
                ret_data = common_cd_ini_test(mode);
                if (0x9000 != ret_data) return ret_data;
            }
            byte[] apdu_test = SE_EMK_match;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);

            string temp_string = byte_to_hexstr(receive_test, (int)receive_test_length);
            PS_to_DBB("Response:" + temp_string + "\r\n");
            if (2 > receive_test_length)
            {
                PS_to_DBB("Response error.\r\n");
                return ret_data;
            }
            ret_data = check_ret_data((uint)(receive_test[2] + (receive_test[3] * 256)));
            if (0x6300 == ret_data)
            {
                PS_to_DBB("Result: Unmatched\r\n");
                return ret_data;
            }
            if (0x9000 != ret_data)
            {

                PS_to_DBB("Result: Verify error\r\n");
                return ret_data;
            }
            byte finger = receive_test[7];
            byte template = receive_test[6];
            byte result = receive_test[receive_test_length - 3];
            if (0x2f != result)
            {
                PS_to_DBB("Result: Unmatched\r\n");
                return ret_data;

            }
            string signature = temp_string.Substring(80, 14);
            PS_to_DBB("Result: Matched\r\n");
            PS_to_DBB("Finger:");
            print_data_to_debugbox(finger + 1);
            PS_to_DBB("\r\n");
            PS_to_DBB("Template:");
            print_data_to_debugbox(template + 1);
            PS_to_DBB("\r\n");
            PS_to_DBB("Signature:" + signature + "\r\n");


            return 0x9000;
        }


        public static uint list_all_record()
        {
            result_after_issue_cmd = 0x6969;
            byte[] apdu_test = list_records;
            uint apdu_length = (uint)list_records.Length;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            result_after_issue_cmd = connect_the_card();
            if (0x9000 != result_after_issue_cmd) return result_after_issue_cmd;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            //Console.WriteLine(ret_data);
            if (2 > receive_test_length)
            {
                PS_to_DBB("Response error.\r\n");
                return result_after_issue_cmd;
            }

            result_after_issue_cmd = check_ret_data((uint)(receive_test[2] + (receive_test[3] * 256)));
            if (0x6a83 == result_after_issue_cmd)
            {
                PS_to_DBB("No record found\r\n");
                return 0x9000;
            }
            if (0x9000 != result_after_issue_cmd) return result_after_issue_cmd;
            byte record_number = receive_test[6];
            string record_string = "";
            for (int i = 0; i < record_number; i++)
            {
                record_string += "0x" + receive_test[8 + i * 2].ToString("x2") + "  ";
            }
            PS_to_DBB("Record:" + record_string + "\r\n");

            return 0x9000;
        }


        public static uint demo_single_enroll()
        {
            result_after_issue_cmd = 0x6969;
            string finger_num_string = Program.mainform.textBox19.Text;
            string template_num_string = Program.mainform.textBox11.Text;
            byte finger_number;
            byte template_number;
            if (("" == finger_num_string) || ("" == template_num_string))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            try
            {
                finger_number = Convert.ToByte(finger_num_string.Substring(0, 1), 10);
                template_number = Convert.ToByte(template_num_string.Substring(0, 1), 10);
            }
            catch (Exception ex)
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                PS_to_DBB(ex.Message + "\r\n");
                return result_after_issue_cmd;
            }
            if ((1 != finger_number) && (2 != finger_number))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            if ((0 == template_number) || (6 < template_number))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            byte template_index = 0;
            try
            {
                template_index = Convert.ToByte((finger_num_string + template_num_string).Substring(0, 2), 16);
            }
            catch
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }

            List<byte> temp_array_list = new List<byte>(SE_EMK_single_enroll);
            temp_array_list.Add(template_index);
            temp_array_list.Add(0x01);
            temp_array_list.Add(0x26);
            byte[] apdu_test = temp_array_list.ToArray();
            uint apdu_length = (uint)temp_array_list.Count;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            result_after_issue_cmd = connect_the_card();
            if (0x9000 != result_after_issue_cmd) return result_after_issue_cmd;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);

            show_Reader_response(receive_test, (int)receive_test_length);

            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            return result_after_issue_cmd;
        }



        public static uint delete_single_template()
        {
            result_after_issue_cmd = 0x6969;
            string finger_num_string = Program.mainform.textBox19.Text;
            string template_num_string = Program.mainform.textBox11.Text;
            byte finger_number;
            byte template_number;
            if (("" == finger_num_string) || ("" == template_num_string))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            try
            {
                finger_number = Convert.ToByte(finger_num_string.Substring(0, 1), 10);
                template_number = Convert.ToByte(template_num_string.Substring(0, 1), 10);
            }
            catch (Exception ex)
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                PS_to_DBB(ex.Message + "\r\n");
                return result_after_issue_cmd;
            }
            if ((1 != finger_number) && (2 != finger_number))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            if ((0 == template_number) || (6 < template_number))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            byte template_index = (byte)((finger_number - 1) * 6 + template_number - 1);
            List<byte> temp_array_list = new List<byte>(delete_template);
            temp_array_list.Add(template_index);
            temp_array_list.Add(0x00);
            byte[] apdu_test = temp_array_list.ToArray();
            uint apdu_length = (uint)temp_array_list.Count;
            uint receive_test_length = 264;
            byte[] receive_test = new byte[264];
            result_after_issue_cmd = connect_the_card();
            if (0x9000 != result_after_issue_cmd) return result_after_issue_cmd;
            result_after_issue_cmd = 0x6969;
            result_after_issue_cmd = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            result_after_issue_cmd = check_ret_data(result_after_issue_cmd);
            return result_after_issue_cmd;
        }


        public delegate void myinvoke2(ComboBox cb);
        public static void go_selected_cmd(ComboBox cb)
        {
            if (cb.InvokeRequired)
            {
                myinvoke2 mi = new myinvoke2(go_selected_cmd);
                Program.mainform.Invoke(mi, new object[] { cb });
            }
            else
            {
                run_selected_cmd(cb.Text, ref result_after_issue_cmd);
                //Console.WriteLine(result_after_issue_cmd);
            }
        }

        public static void run_selected_cmd(string selected_cmd_name, ref uint return_data)
        {

            switch (selected_cmd_name)
            {
                case "None":
                    {
                        PS_to_DBB("No access\r\n");
                        return_data = 0x9000;
                    }
                    break;
                //"Get Attrib String"
                case "Get Attrib String":
                    {
                        return_data = get_scard_atr();
                    }
                    break;
                case "Return BL mode":
                    {
                        return_to_bl_mode();
                        return_data = result_after_issue_cmd;
                    }
                    break;
                case "Program_update_bin":
                    {
                        LoadUpdateBinToMcu();
                        return_data = result_after_issue_cmd;
                    }
                    break;
                case "Program_se":
                    {
                        update_SE_FW();
                        return_data = result_after_issue_cmd;
                    }
                    break;
                    
                case "FM1280 program SE":
                    {
                        FM_update_SE_FW();
                        return_data = result_after_issue_cmd;
                    }
                    break;
                case "Update_BMCU_FW":
                    {
                        LoadMcuFWBinToMcu();
                        return_data = result_after_issue_cmd;
                    }
                    break;
                case "Update_sensor_fw"://Update_sensor_fw
                    {
                        return_data = common_cd_ini_test(0x02);
                        if ((0x9000 != return_data) && (0x6d00 != return_data)) break;
                        return_data = delete_database();
                        if ((0x9000 != return_data) && (0x6d00 != return_data)) break;
                        LoadUpdateBinToMcu();
                        return_data = result_after_issue_cmd;
                        if ((0x9000 != result_after_issue_cmd) && (0x6d00 != result_after_issue_cmd)) break;
                        LoadSensorUtilityBinToMcu();
                        return_data = result_after_issue_cmd;
                        if ((0x9000 != result_after_issue_cmd) && (0x6d00 != result_after_issue_cmd)) break;
                        LoadSensorPrimaryBinToMcu();
                        return_data = result_after_issue_cmd;
                        if (0x9000 != return_data) break;
                    }
                    break;
                case "BMCU_integration_update":
                    {
                        return_data = common_cd_ini_test(0x00);
                        if ((0x9000 != return_data) && (0x6d00 != return_data)) break;
                        return_data = delete_database();
                        if ((0x9000 != return_data) && (0x6d00 != return_data)) break;
                        LoadUpdateBinToMcu();
                        return_data = result_after_issue_cmd;
                        //Console.WriteLine("1");
                        //Console.WriteLine(return_data);
                        if ((0x9000 != result_after_issue_cmd) && (0x6d00 != result_after_issue_cmd)) break;
                        LoadMcuFWBinToMcu();
                        return_data = result_after_issue_cmd;
                        //Console.WriteLine("2");
                        //Console.WriteLine(return_data);
                        if (0x9000 != result_after_issue_cmd) break;
                    }
                    break;
                case "Contacted initial test":
                    {
                        //common_cd_un_ini_test();
                        return_data = common_cd_ini_test(0x02);
                    }
                    break;
                case "Contactless initial test":
                    {
                        //common_cd_un_ini_test();
                        return_data = common_cd_ini_test(0x03);
                    }
                    break;
                case "No sensor cd initial test":
                    {
                        //common_cd_un_ini_test();
                        return_data = common_cd_ini_test(0x00);
                    }
                    break;
                case "No sensor cl initial test":
                    {
                        //common_cd_un_ini_test();
                        return_data = common_cd_ini_test(0x01);
                    }
                    break;
                case "capture image wff":
                    {
                        return_data = image_capture(0x03);
                    }
                    break;
                case "capture image live":
                    {
                        return_data = image_capture(0x01);
                    }
                    break;
                case "Display image wff":
                    {
                        image_to_pc(0x03);
                        return_data = result_after_issue_cmd;
                    }
                    break;
                case "Display image live":
                    {
                        image_to_pc(0x01);
                        return_data = result_after_issue_cmd;
                    }
                    break;
                case "Demo 1 finger enrollment":
                    {
                        return_data = enroll_one_finger(1);
                    }
                    break;
                case "Demo 2 finger enrollment":
                    {
                        return_data = enroll_one_finger(2);
                    }
                    break;
                //
                case "Demo single enrollment":
                    {
                        return_data = demo_single_enroll();
                    }
                    break;
                case "Demo verify contact":
                    {
                        return_data = verify_finger(2);
                    }
                    break;
                case "Demo verify contactless":
                    {
                        return_data = verify_finger(3);
                    }
                    break;
                case "Demo secure verify contact":
                    {
                        return_data = secure_verify_finger(1);
                    }
                    break;
                case "Demo secure verify contactless":
                    {
                        return_data = secure_verify_finger(1);
                    }
                    break;
                case "List all record":
                    {
                        return_data = list_all_record();
                    }
                    break;
                case "Delete record":
                    {
                        return_data = delete_database();
                    }
                    break;

                case "Delete single template":
                    {
                        return_data = delete_single_template();
                    }
                    break;
                case "Reset key":
                    {
                        return_data = reset_secure_key();
                    }
                    break;
                case "Version check":
                    {
                        return_data = version_check();
                    }
                    break;
                case "output image from card":
                    {
                        transfer_image_to_pc();
                        return_data = result_after_issue_cmd;
                    }
                    break;
                case "load image to card":
                    {
                        load_image_for_verify();
                        return_data = result_after_issue_cmd;
                    }
                    break;
                case "verify load image":
                    {
                        return_data = secure_verify_finger(0);// verify_load_image();
                    }
                    break;
                case "Force calibration":
                    {
                        return_data = Force_cal_and_store(0x01);
                    }
                    break;
                case "Force calibration and store":
                    {
                        return_data = Force_cal_and_store(0x09);
                    }
                    break;
                case "Get ID":
                    {
                        return_data = get_all_id();
                    }
                    break;
                case "set factory keys"://set_factory_keys_V256
                    {
                        return_data = set_factory_key();
                    }
                    break;
                case "set_factory_keys_V256"://set_factory_keys_V256
                    {
                        return_data = set_factory_key_v256();
                    }
                    break;
                case "open SCP03"://open_SCP03_v256
                    {
                        return_data = open_scp03();
                    }
                    break;
                case "open_SCP03_v256"://open_SCP03_v256
                    {
                        return_data = open_scp03_v256();
                    }
                    break;
                case "FM1280 return BL":
                    {
                        FM1280_helper.FM_return_to_bl_mode();
                        return_data = result_after_issue_cmd;
                    } break;
                case "FM1280 return app":
                    {
                        return_data = FM1280_return_app();
                    }
                    break;
                case "IMM_get_template":
                    {
                        return_data = IMM_get_template();
                    }
                    break;
                case "IMM_merge_template":
                    {
                        return_data = IMM_merge_template();
                    }
                    break;
                case "IMM_store_template":
                    {
                        return_data = IMM_store_template();
                    }
                    break;
                case "IMM_list_record":
                    {
                        return_data = IMM_list_record();
                    }
                    break;
                case "IMM_verify_template":
                    {
                        return_data = IMM_match_template();
                    }
                    break;
                case "FM1280_read_enroll_template":
                    {
                        return_data = fm1280_read_enrollment_template();
                    }
                    break;
                case "FM1280_read_verify_template":
                    {
                        return_data = fm1280_read_verify_template();
                    }
                    break;
                case "fm1280_temo_4k_data":
                    {
                        return_data = fm1280_read_tempo_4k();
                    }
                    break;
                case "fm1280_6k_data_after_index_table":
                    {
                        return_data = fm1280_read_6k_data_after_index_table();
                    }
                    break;
                case "fm1280_4k_ram1_after_match":
                    {
                        return_data = fm1280_read_4k_ram1_after_match();
                    }
                    break;
                case "erase_debug_flash_block":
                    {
                        return_data = erase_debug_block();
                    }
                    break;
                case "read_data_from_flash":
                    {
                        return_data = read_flash_from_address();
                    }
                    break;
                default: break;
            }
            //return return_data;
        }//imm_match_templates

        
        public static uint read_flash_from_address()
        {
            int i;
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = read_data_from_flash;

            string falsh_address_string = Program.mainform.textBox3.Text;
            string read_size_string = Program.mainform.textBox20.Text;
            if (("" == falsh_address_string) || ("" == falsh_address_string))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return ret_data;
            }
            UInt32 flash_address = 0;
            UInt32 read_size = 0;
            int address_string_size = falsh_address_string.Length;
            int size_string_size = read_size_string.Length;
            try
            {

                flash_address = Convert.ToUInt32(falsh_address_string.Substring(0, address_string_size), 16);
                read_size = Convert.ToUInt32(read_size_string.Substring(0, size_string_size), 16);
            }
            catch (Exception ex)
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                PS_to_DBB(ex.Message + "\r\n");
                return ret_data;
            }
            /*
            Console.WriteLine(falsh_address_string);
            Console.WriteLine(read_size_string);
            Console.WriteLine(flash_address);
            Console.WriteLine(read_size);
            */

            
            UInt32 address_offset = flash_address;
            while (0<read_size)
            {
                apdu_test[5] = (byte)((address_offset & 0xff000000) >> 24);
                apdu_test[6] = (byte)((address_offset & 0x00ff0000) >> 16);
                apdu_test[7] = (byte)((address_offset & 0x0000ff00) >> 8);
                apdu_test[8] = (byte)(address_offset & 0x000000ff);
                ret_data = connect_the_card();
                if (0x9000 != ret_data) return ret_data;
                ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
                if (0x9000 != ret_data) return ret_data;
                data_array_print(receive_test, (int)receive_test_length);
                address_offset += 128;
                if (128 <= read_size)
                { read_size -= 128; }
                else
                { read_size = 0; }
            }
 
            return 0x9000;
        }


        public static void data_array_print(byte[] data_array, int length)
        {
            int i;
            if (8 >= length)
            {
                PS_to_DBB("data length is not correct.\r\n");
                return;
            }
            PS_to_DBB("Address: ");
            byte[] temp_array = new byte[4];
            temp_array[0] = data_array[4];
            temp_array[1] = data_array[5];
            temp_array[2] = data_array[6];
            temp_array[3] = data_array[7];
            string temp_string = byte_to_hexstr(temp_array, 4);
            PS_to_DBB(temp_string + "\r\n");
            //print address
            byte[] temp2_array = new byte[length];
            for (i = 8; i < length - 2; i++)
            {
                temp2_array[i - 8] = data_array[i];
            }
            temp_string = byte_to_hexstr(temp2_array, (length - 10));
            PS_to_DBB(temp_string + "\r\n");
            return;
        }


        public static uint erase_debug_block()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = erase_flash_debug_block;

            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            if (0x9000 != ret_data) return ret_data;
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }




        public static void fm1280_enroll_data_print(byte[] response, int length)
        {
            int i;
            if (6 >= length)
            {
                PS_to_DBB("data length is not correct.\r\n");
                return;
            }
            PS_to_DBB("Address: ");
            byte[] temp_array = new byte[4];
            temp_array[0] = response[3];
            temp_array[1] = response[2];
            temp_array[2] = response[1];
            temp_array[3] = response[0];
            string temp_string = byte_to_hexstr(temp_array, 4);
            PS_to_DBB(temp_string + "\r\n");
            //print address
            byte[] temp2_array = new byte[length];
            for (i = 4; i < length-2; i++)
            {
                temp2_array[i - 4] = response[i];
            }
            temp_string= byte_to_hexstr(temp2_array, (length-6));
            PS_to_DBB(temp_string + "\r\n");
            return ;
        }


        public static uint fm1280_read_6k_data_after_index_table()
        {
            uint ret_data = 0x6969;
            int i;
            UInt16 base_flash_address;
            UInt16 current_flash_address;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = FM1280_read_template;
            //extract data,size
            base_flash_address = 0xd800;//size
            PS_to_DBB("6k_data_after_index_table:\r\n");

            for (i = 0; i < 48; i++)
            {
                current_flash_address = (UInt16)(base_flash_address + (i * 128));
                apdu_test[2] = (byte)(current_flash_address & 0x00ff);
                apdu_test[3] = (byte)((current_flash_address & 0xff00) >> 8);
                ret_data = connect_the_card();
                if (0x9000 != ret_data) return ret_data;
                ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
                if (0x9000 != ret_data) return ret_data;
                fm1280_enroll_data_print(receive_test, (int)receive_test_length);
            }

            return ret_data;


        }

        public static uint fm1280_read_4k_ram1_after_match()
        {
            uint ret_data = 0x6969;
            int i;
            UInt16 base_flash_address;
            UInt16 current_flash_address;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = FM1280_read_template;
            //extract data,size
            base_flash_address = 0x9800;//size
            PS_to_DBB("Ram1 data after matching:\r\n");

            for (i = 0; i < 32; i++)
            {
                current_flash_address = (UInt16)(base_flash_address + (i * 128));
                apdu_test[2] = (byte)(current_flash_address & 0x00ff);
                apdu_test[3] = (byte)((current_flash_address & 0xff00) >> 8);
                ret_data = connect_the_card();
                if (0x9000 != ret_data) return ret_data;
                ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
                if (0x9000 != ret_data) return ret_data;
                fm1280_enroll_data_print(receive_test, (int)receive_test_length);
            }

            return ret_data;


        }

        public static uint fm1280_read_tempo_4k()
        {
            uint ret_data = 0x6969;
            int i;
            UInt16 base_flash_address;
            UInt16 current_flash_address;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = FM1280_read_template;
            //extract data,size
            base_flash_address = 0x8800;//size
            PS_to_DBB("verify data part1, ccSE_Data->SE_FlashLocation_1_Ptr:\r\n");

            for (i = 0; i < 32; i++)
            {
                current_flash_address = (UInt16)(base_flash_address + (i * 128));
                apdu_test[2] = (byte)(current_flash_address & 0x00ff);
                apdu_test[3] = (byte)((current_flash_address & 0xff00) >> 8);
                ret_data = connect_the_card();
                if (0x9000 != ret_data) return ret_data;
                ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
                if (0x9000 != ret_data) return ret_data;
                fm1280_enroll_data_print(receive_test, (int)receive_test_length);
            }

            return ret_data;


        }

        public static uint fm1280_read_verify_template()
        {
            uint ret_data = 0x6969;
            int i;
            UInt16 base_flash_address;
            UInt16 current_flash_address;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = FM1280_read_template;
            //extract data,size
            base_flash_address = 0xa800;//size
            PS_to_DBB("verify data part1, extract data:\r\n");
            PS_to_DBB("data size: ");
            apdu_test[2] = (byte)(base_flash_address & 0x00ff);
            apdu_test[3] = (byte)((base_flash_address & 0xff00) >> 8);
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            if (0x9000 != ret_data) return ret_data;
            byte[] size_buffer = new byte[4];
            size_buffer[0] = receive_test[4];
            size_buffer[1] = receive_test[5];
            string temp_string= byte_to_hexstr(size_buffer, 2);
            PS_to_DBB("0x"+ temp_string + "\r\n");
            //data
            base_flash_address = 0xa900;
            for (i = 0; i < 4; i++)
            {
                current_flash_address = (UInt16)(base_flash_address + (i * 128));
                apdu_test[2] = (byte)(current_flash_address & 0x00ff);
                apdu_test[3] = (byte)((current_flash_address & 0xff00) >> 8);
                ret_data = connect_the_card();
                if (0x9000 != ret_data) return ret_data;
                ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
                if (0x9000 != ret_data) return ret_data;
                fm1280_enroll_data_print(receive_test, (int)receive_test_length);
            }
            //verify data part2, data 1
            base_flash_address = 0xb800;
            PS_to_DBB("verify data part2, processed data1:\r\n");
            PS_to_DBB("data size: ");
            apdu_test[2] = (byte)(base_flash_address & 0x00ff);
            apdu_test[3] = (byte)((base_flash_address & 0xff00) >> 8);
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            if (0x9000 != ret_data) return ret_data;
            size_buffer[0] = receive_test[4];
            size_buffer[1] = receive_test[5];
            temp_string = byte_to_hexstr(size_buffer, 2);
            PS_to_DBB("0x" + temp_string + "\r\n");
            //data
            base_flash_address = 0xb900;
            for (i = 0; i < 32; i++)
            {
                current_flash_address = (UInt16)(base_flash_address + (i * 128));
                apdu_test[2] = (byte)(current_flash_address & 0x00ff);
                apdu_test[3] = (byte)((current_flash_address & 0xff00) >> 8);
                ret_data = connect_the_card();
                if (0x9000 != ret_data) return ret_data;
                ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
                if (0x9000 != ret_data) return ret_data;
                fm1280_enroll_data_print(receive_test, (int)receive_test_length);
            }
            //verify data part3, data 1
            base_flash_address = 0xc800;
            PS_to_DBB("verify data part3, processed data2:\r\n");
            PS_to_DBB("data size: ");
            apdu_test[2] = (byte)(base_flash_address & 0x00ff);
            apdu_test[3] = (byte)((base_flash_address & 0xff00) >> 8);
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            if (0x9000 != ret_data) return ret_data;
            size_buffer[0] = receive_test[4];
            size_buffer[1] = receive_test[5];
            temp_string = byte_to_hexstr(size_buffer, 2);
            PS_to_DBB("0x" + temp_string + "\r\n");
            //data
            base_flash_address = 0xc900;
            for (i = 0; i < 4; i++)
            {
                current_flash_address = (UInt16)(base_flash_address + (i * 128));
                apdu_test[2] = (byte)(current_flash_address & 0x00ff);
                apdu_test[3] = (byte)((current_flash_address & 0xff00) >> 8);
                ret_data = connect_the_card();
                if (0x9000 != ret_data) return ret_data;
                ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
                if (0x9000 != ret_data) return ret_data;
                fm1280_enroll_data_print(receive_test, (int)receive_test_length);
            }

            return ret_data;
        }
        public static uint fm1280_read_enrollment_template()
        {
            uint ret_data = 0x6969;
            int i;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = FM1280_read_template;
            string finger_num_string = Program.mainform.textBox19.Text;
            string template_num_string = Program.mainform.textBox11.Text;
            byte finger_number;
            byte template_number;
            if (("" == finger_num_string) || ("" == template_num_string))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            try
            {
                finger_number = Convert.ToByte(finger_num_string.Substring(0, 1), 10);
                template_number = Convert.ToByte(template_num_string.Substring(0, 1), 10);
            }
            catch (Exception ex)
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                PS_to_DBB(ex.Message + "\r\n");
                return result_after_issue_cmd;
            }
            if ((1 != finger_number) && (2 != finger_number))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            if ((0 == template_number) || (6 < template_number))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            PS_to_DBB("finger "+ finger_num_string +"template "+ template_num_string + "\r\n");
            byte template_index = 0;
            template_index = (byte)((finger_number - 1) * 6 + template_number - 1);
            UInt16 base_flash_address = 0x1200;
            UInt16 current_flash_address = 0x0000;
            base_flash_address = (UInt16)(0x1200 + (template_index* 0x0800));
            PS_to_DBB("Enrollment data part1:\r\n");
            for (i=0;i<4;i++)
            {
                current_flash_address = (UInt16)(base_flash_address + (i * 128));
                apdu_test[2] = (byte)(current_flash_address & 0x00ff);
                apdu_test[3] = (byte)((current_flash_address & 0xff00)>>8);
                ret_data = connect_the_card();
                if (0x9000 != ret_data) return ret_data;
                ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
                if (0x9000 != ret_data) return ret_data;
                fm1280_enroll_data_print(receive_test, (int)receive_test_length);
            }
            base_flash_address = (UInt16)(0x1200 + (template_index * 0x0800)+768);
            PS_to_DBB("Enrollment data part2:\r\n");
            for (i = 0; i < 10; i++)
            {
                current_flash_address = (UInt16)(base_flash_address + (i * 128));
                apdu_test[2] = (byte)(current_flash_address & 0x00ff);
                apdu_test[3] = (byte)((current_flash_address & 0xff00) >> 8);
                ret_data = connect_the_card();
                if (0x9000 != ret_data) return ret_data;
                ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
                if (0x9000 != ret_data) return ret_data;
                fm1280_enroll_data_print(receive_test, (int)receive_test_length);
            }

            return ret_data;
        }





        public static uint IMM_match_template()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = imm_match_templates;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }

        public static uint IMM_list_record()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = imm_list_records;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }

        public static uint IMM_get_template()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = imm_get_template;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }
        public static uint IMM_merge_template()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            byte[] apdu_test = imm_merge_templates;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }
        public static uint IMM_store_template()
        {
            uint ret_data = 0x6969;
            uint receive_test_length = 0;
            byte[] receive_test = new byte[264];
            string finger_num_string = Program.mainform.textBox19.Text;
            byte finger_ID;
            if ("" == finger_num_string)
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return result_after_issue_cmd;
            }
            try
            {
                finger_ID = Convert.ToByte(finger_num_string.Substring(0, 1), 10);
            }
            catch (Exception ex)
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                PS_to_DBB(ex.Message + "\r\n");
                return ret_data;
            }
            if ((1 != finger_ID) && (2 != finger_ID))
            {
                PS_to_DBB("Appoint data is not correct.\r\n");
                return ret_data;
            }
            byte[] apdu_test = imm_store_template;
            apdu_test[8] = finger_ID;
            ret_data = connect_the_card();
            if (0x9000 != ret_data) return ret_data;
            ret_data = common_send_apdu(apdu_test, (uint)apdu_test.Length, ref receive_test, ref receive_test_length);
            show_Reader_response(receive_test, (int)receive_test_length);
            ret_data = check_ret_data(ret_data);
            return ret_data;
        }

        public static int ng_counter = 0;



        public static void start_selected_test()
        {
            ng_counter = 0;
            Program.mainform.initial_UI();
            Application.DoEvents();
            PS_to_DBB("Start testing\r\n");
            if (true == Program.mainform.checkBox6.Checked)
            {
                //Thread.Sleep(5000);
                go_selected_cmd(Program.mainform.comboBox1);
                //Console.WriteLine(result_after_issue_cmd);
                //Thread.Sleep(5000);
                if (0x9000 == result_after_issue_cmd)
                {
                    //label10.Image = THD89_CRD_Tester.Properties.Resources.GREEN;
                    Program.mainform.change_label_image(Program.mainform.label10, CRD_APDU_Tester.Properties.Resources.GREEN);
                }
                else
                {
                    if (0 != result_after_issue_cmd)
                    {
                        //label10.Image = THD89_CRD_Tester.Properties.Resources.RED;
                        Program.mainform.change_label_image(Program.mainform.label10, CRD_APDU_Tester.Properties.Resources.RED);
                        ng_counter = ng_counter + 1;
                    }
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            if (true == Program.mainform.checkBox7.Checked)
            {
                go_selected_cmd(Program.mainform.comboBox2);
                if (0x9000 == result_after_issue_cmd)
                {
                    //label11.Image = THD89_CRD_Tester.Properties.Resources.GREEN;
                    Program.mainform.change_label_image(Program.mainform.label11, CRD_APDU_Tester.Properties.Resources.GREEN);
                }
                else
                {
                    if (0 != result_after_issue_cmd)
                    {
                        //label11.Image = THD89_CRD_Tester.Properties.Resources.RED;
                        Program.mainform.change_label_image(Program.mainform.label11, CRD_APDU_Tester.Properties.Resources.RED);
                        ng_counter = ng_counter + 1;
                    }
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            if (true == Program.mainform.checkBox8.Checked)
            {
                go_selected_cmd(Program.mainform.comboBox3);
                if (0x9000 == result_after_issue_cmd)
                {
                    //label12.Image = THD89_CRD_Tester.Properties.Resources.GREEN;
                    Program.mainform.change_label_image(Program.mainform.label12, CRD_APDU_Tester.Properties.Resources.GREEN);
                }
                else
                {
                    if (0 != result_after_issue_cmd)
                    {
                        //label12.Image = THD89_CRD_Tester.Properties.Resources.RED;
                        Program.mainform.change_label_image(Program.mainform.label12, CRD_APDU_Tester.Properties.Resources.RED);
                        ng_counter = ng_counter + 1;
                    }
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            if (true == Program.mainform.checkBox9.Checked)
            {
                go_selected_cmd(Program.mainform.comboBox4);
                if (0x9000 == result_after_issue_cmd)
                {
                    //label13.Image = THD89_CRD_Tester.Properties.Resources.GREEN;
                    Program.mainform.change_label_image(Program.mainform.label13, CRD_APDU_Tester.Properties.Resources.GREEN);
                }
                else
                {
                    if (0 != result_after_issue_cmd)
                    {
                        //label13.Image = THD89_CRD_Tester.Properties.Resources.RED;
                        Program.mainform.change_label_image(Program.mainform.label13, CRD_APDU_Tester.Properties.Resources.RED);
                        ng_counter = ng_counter + 1;
                    }
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            if (true == Program.mainform.checkBox10.Checked)
            {
                go_selected_cmd(Program.mainform.comboBox5);
                if (0x9000 == result_after_issue_cmd)
                {
                    //label14.Image = THD89_CRD_Tester.Properties.Resources.GREEN;
                    Program.mainform.change_label_image(Program.mainform.label14, CRD_APDU_Tester.Properties.Resources.GREEN);
                }
                else
                {
                    if (0 != result_after_issue_cmd)
                    {
                        //label14.Image = THD89_CRD_Tester.Properties.Resources.RED;
                        Program.mainform.change_label_image(Program.mainform.label14, CRD_APDU_Tester.Properties.Resources.RED);
                        ng_counter = ng_counter + 1;
                    }
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            if (true == Program.mainform.checkBox11.Checked)
            {
                go_selected_cmd(Program.mainform.comboBox6);
                if (0x9000 == result_after_issue_cmd)
                {
                    //label15.Image = THD89_CRD_Tester.Properties.Resources.GREEN;
                    Program.mainform.change_label_image(Program.mainform.label15, CRD_APDU_Tester.Properties.Resources.GREEN);
                }
                else
                {
                    if (0 != result_after_issue_cmd)
                    {
                        //label15.Image = THD89_CRD_Tester.Properties.Resources.RED;
                        Program.mainform.change_label_image(Program.mainform.label15, CRD_APDU_Tester.Properties.Resources.RED);
                        ng_counter = ng_counter + 1;
                    }
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            if (true == Program.mainform.checkBox12.Checked)
            {
                go_selected_cmd(Program.mainform.comboBox7);
                if (0x9000 == result_after_issue_cmd)
                {
                    //label16.Image = THD89_CRD_Tester.Properties.Resources.GREEN;
                    Program.mainform.change_label_image(Program.mainform.label16, CRD_APDU_Tester.Properties.Resources.GREEN);
                }
                else
                {
                    if (0 != result_after_issue_cmd)
                    {
                        //label16.Image = THD89_CRD_Tester.Properties.Resources.RED;
                        Program.mainform.change_label_image(Program.mainform.label16, CRD_APDU_Tester.Properties.Resources.RED);
                        ng_counter = ng_counter + 1;
                    }
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            if (true == Program.mainform.checkBox13.Checked)
            {
                go_selected_cmd(Program.mainform.comboBox8);
                if (0x9000 == result_after_issue_cmd)
                {
                    //label17.Image = THD89_CRD_Tester.Properties.Resources.GREEN;
                    Program.mainform.change_label_image(Program.mainform.label17, CRD_APDU_Tester.Properties.Resources.GREEN);
                }
                else
                {
                    if (0 != result_after_issue_cmd)
                    {
                        //label17.Image = THD89_CRD_Tester.Properties.Resources.RED;
                        Program.mainform.change_label_image(Program.mainform.label17, CRD_APDU_Tester.Properties.Resources.RED);
                        ng_counter = ng_counter + 1;
                    }
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            if (true == Program.mainform.checkBox14.Checked)
            {
                Send_customize_apdu();
                if (0x9000 == result_after_issue_cmd)
                {
                    //label18.Image = THD89_CRD_Tester.Properties.Resources.GREEN;
                    Program.mainform.change_label_image(Program.mainform.label18, CRD_APDU_Tester.Properties.Resources.GREEN);
                }
                else
                {
                    if (0 != result_after_issue_cmd)
                    {
                        //label18.Image = THD89_CRD_Tester.Properties.Resources.RED;
                        Program.mainform.change_label_image( Program.mainform.label18, CRD_APDU_Tester.Properties.Resources.RED);
                        ng_counter = ng_counter + 1;
                    }
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(100);
            }
            if (0 < ng_counter)
            {
                //label19.Image = THD89_CRD_Tester.Properties.Resources.NG;
                Program.mainform.change_label_image(Program.mainform.label19, CRD_APDU_Tester.Properties.Resources.NG);
            }
            else
            {
                //label19.Image = THD89_CRD_Tester.Properties.Resources.OK;
                Program.mainform.change_label_image(Program.mainform.label19, CRD_APDU_Tester.Properties.Resources.OK);
            }
            Application.DoEvents();
            Program.mainform.is_start_test = 0;
        }


        public static void load_config_object(string[] option)
        {
            switch (option[0])
            {
                case "SE_FW":
                    {
                        Program.mainform.textBox5.Text = option[1];
                        SE_FW_filename = option[1];
                    }
                    break;
                case "Update_bin":
                    {
                        Program.mainform.textBox6.Text = option[1];
                        update_bin_filename = option[1];

                    }
                    break;
                case "MCU_FW":
                    {
                        Program.mainform.textBox7.Text = option[1];
                        mcu_fw_filename = option[1];
                        if ("" != mcu_fw_filename)
                        {
                            get_hash(mcu_fw_filename, mcu_fw_hash);
                            //textBox1.Text = byte_to_hexstr(mcu_fw_hash, 16);
                            PS_to_DBB("MCU FW hash:" + byte_to_hexstr(mcu_fw_hash, 16) + "\r\n");
                        }
                    }
                    break;
                case "Sensor_FW_pri":
                    {
                        Program.mainform.textBox8.Text = option[1];
                        sensor_pri_fw_filename = option[1];
                        if ("" != sensor_pri_fw_filename)
                        {
                            get_hash(sensor_pri_fw_filename, sensor_pri_fw_hash);
                            //textBox2.Text = byte_to_hexstr(sensor_pri_fw_hash, 16);
                            PS_to_DBB("Sensor FW hash:" + byte_to_hexstr(sensor_pri_fw_hash, 16) + "\r\n");
                        }
                    }
                    break;
                case "Sensor_FW_uti":
                    {
                        Program.mainform.textBox9.Text = option[1];
                        sensor_uti_fw_filename = option[1];
                        if ("" != sensor_uti_fw_filename)
                        {
                            get_hash(sensor_uti_fw_filename, sensor_uti_fw_hash);
                            //textBox3.Text = byte_to_hexstr(sensor_uti_fw_hash, 16);
                            PS_to_DBB("Sensor BL hash:" + byte_to_hexstr(sensor_uti_fw_hash, 16) + "\r\n");
                        }
                    }
                    break;
                //-----------------------------------file area
                case "log_enable":
                    {
                        if ("True".ToLower() == option[1].ToLower()) { Program.mainform.checkBox1.Checked = true; }
                        else { Program.mainform.checkBox1.Checked = false; }
                    }
                    break;
                case "image_invert":
                    {
                        if ("True".ToLower() == option[1].ToLower()) { Program.mainform.checkBox2.Checked = true; }
                        else { Program.mainform.checkBox2.Checked = false; }
                    }
                    break;
                //-------------------------------------option area
                case "Load_image":
                    {
                        Program.mainform.textBox10.Text = option[1];
                    }
                    break;
                //--------------------------------------load image area
                case "Command_1":
                    {
                        Program.mainform.comboBox1.SelectedIndex = Convert.ToInt32(option[1]);
                        if ("True".ToLower() == option[2].ToLower()) { Program.mainform.checkBox6.Checked = true; }
                        else { Program.mainform.checkBox6.Checked = false; }
                    }
                    break;
                case "Command_2":
                    {
                        Program.mainform.comboBox2.SelectedIndex = Convert.ToInt32(option[1]);
                        if ("True".ToLower() == option[2].ToLower()) { Program.mainform.checkBox7.Checked = true; }
                        else { Program.mainform.checkBox7.Checked = false; }
                    }
                    break;
                case "Command_3":
                    {
                        Program.mainform.comboBox3.SelectedIndex = Convert.ToInt32(option[1]);
                        if ("True".ToLower() == option[2].ToLower()) { Program.mainform.checkBox8.Checked = true; }
                        else { Program.mainform.checkBox8.Checked = false; }
                    }
                    break;
                case "Command_4":
                    {
                        Program.mainform.comboBox4.SelectedIndex = Convert.ToInt32(option[1]);
                        if ("True".ToLower() == option[2].ToLower()) { Program.mainform.checkBox9.Checked = true; }
                        else { Program.mainform.checkBox9.Checked = false; }
                    }
                    break;
                case "Command_5":
                    {
                        Program.mainform.comboBox5.SelectedIndex = Convert.ToInt32(option[1]);
                        if ("True".ToLower() == option[2].ToLower()) { Program.mainform.checkBox10.Checked = true; }
                        else { Program.mainform.checkBox10.Checked = false; }
                    }
                    break;
                case "Command_6":
                    {
                        Program.mainform.comboBox6.SelectedIndex = Convert.ToInt32(option[1]);
                        if ("True".ToLower() == option[2].ToLower()) { Program.mainform.checkBox11.Checked = true; }
                        else { Program.mainform.checkBox11.Checked = false; }
                    }
                    break;
                case "Command_7":
                    {
                        Program.mainform.comboBox7.SelectedIndex = Convert.ToInt32(option[1]);
                        if ("True".ToLower() == option[2].ToLower()) { Program.mainform.checkBox12.Checked = true; }
                        else { Program.mainform.checkBox12.Checked = false; }
                    }
                    break;
                case "Command_8":
                    {
                        Program.mainform.comboBox8.SelectedIndex = Convert.ToInt32(option[1]);
                        if ("True".ToLower() == option[2].ToLower()) { Program.mainform.checkBox13.Checked = true; }
                        else { Program.mainform.checkBox13.Checked = false; }
                    }
                    break;
                case "Command_9":
                    {
                        Program.mainform.textBox18.Text = option[1];
                        if ("True".ToLower() == option[2].ToLower()) { Program.mainform.checkBox14.Checked = true; }
                        else { Program.mainform.checkBox14.Checked = false; }
                    }
                    break;
                //----------------------------------------------command area
                case "SE_version":
                    {
                        if ("True".ToLower() == option[1].ToLower()) { Program.mainform.checkBox3.Checked = true; }
                        else { Program.mainform.checkBox3.Checked = false; }
                        Program.mainform.textBox14.Text = option[2];
                    }
                    break;
                case "MCU_version":
                    {
                        if ("True".ToLower() == option[1].ToLower()) { Program.mainform.checkBox4.Checked = true; }
                        else { Program.mainform.checkBox4.Checked = false; }
                        Program.mainform.textBox15.Text = option[2];
                    }
                    break;
                case "Sensor_version":
                    {
                        if ("True".ToLower() == option[1].ToLower()) { Program.mainform.checkBox5.Checked = true; }
                        else { Program.mainform.checkBox5.Checked = false; }
                        Program.mainform.textBox17.Text = option[2];
                    }
                    break;
                //---------------------------------------------version area

                //---------------------------------------------se selection area
                case "Image_format":
                    {
                        Program.mainform.comboBox10.SelectedIndex = Convert.ToInt32(option[1]);
                    }
                    break;
                //---------------------------------------------image format area
                default: break;
            }

        }


        public static void load_config_file(string filefullpath)
        {
            if (!(File.Exists(filefullpath)))
            {
                PS_to_DBB("Config file not found\r\n");
                return;
            }

            StreamReader filestream = null;
            string[] txt_block = null;
            string temp_string = "";
            filestream = new StreamReader(filefullpath, System.Text.Encoding.Default);
            while (!filestream.EndOfStream)
            {
                temp_string = filestream.ReadLine();
                temp_string = temp_string.Replace("\n", "").Replace(" ", "").Replace("\t", "").Replace("\r", "").Replace("#", "");
                if ("" != temp_string)
                {
                    txt_block = temp_string.Split(',');
                    load_config_object(txt_block);

                }

            }
            filestream.Close();
            filestream.Dispose();
            Application.DoEvents();
        }

        public static void save_config_file(string filefullpath)
        {
            FileInfo fi = new FileInfo(filefullpath);
            if (File.Exists(filefullpath))
            {
                File.Delete(filefullpath);
            }
            fi.Directory.Create();
            FileStream fs = new FileStream(filefullpath, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
            string data = "";
            data = "SE_FW" + "," + Program.mainform.textBox5.Text;
            sw.WriteLine(data);
            data = "Update_bin" + "," + Program.mainform.textBox6.Text;
            sw.WriteLine(data);
            data = "MCU_FW" + "," + Program.mainform.textBox7.Text;
            sw.WriteLine(data);
            data = "Sensor_FW_pri" + "," + Program.mainform.textBox8.Text;
            sw.WriteLine(data);
            data = "Sensor_FW_uti" + "," + Program.mainform.textBox9.Text;
            sw.WriteLine(data);
            //----------------------------------file area
            data = "log_enable" + "," + Program.mainform.checkBox1.Checked.ToString();
            sw.WriteLine(data);
            data = "image_invert" + "," + Program.mainform.checkBox2.Checked.ToString();
            sw.WriteLine(data);
            //---------------------------------option area
            data = "Load_image" + "," + Program.mainform.textBox10.Text;
            sw.WriteLine(data);
            //-----------------------------------load image area
            data = "Command_1" + "," + Program.mainform.comboBox1.SelectedIndex.ToString() + "," + Program.mainform.checkBox6.Checked.ToString();
            sw.WriteLine(data);
            data = "Command_2" + "," + Program.mainform.comboBox2.SelectedIndex.ToString() + "," + Program.mainform.checkBox7.Checked.ToString();
            sw.WriteLine(data);
            data = "Command_3" + "," + Program.mainform.comboBox3.SelectedIndex.ToString() + "," + Program.mainform.checkBox8.Checked.ToString();
            sw.WriteLine(data);
            data = "Command_4" + "," + Program.mainform.comboBox4.SelectedIndex.ToString() + "," + Program.mainform.checkBox9.Checked.ToString();
            sw.WriteLine(data);
            data = "Command_5" + "," + Program.mainform.comboBox5.SelectedIndex.ToString() + "," + Program.mainform.checkBox10.Checked.ToString();
            sw.WriteLine(data);
            data = "Command_6" + "," + Program.mainform.comboBox6.SelectedIndex.ToString() + "," + Program.mainform.checkBox11.Checked.ToString();
            sw.WriteLine(data);
            data = "Command_7" + "," + Program.mainform.comboBox7.SelectedIndex.ToString() + "," + Program.mainform.checkBox12.Checked.ToString();
            sw.WriteLine(data);
            data = "Command_8" + "," + Program.mainform.comboBox8.SelectedIndex.ToString() + "," + Program.mainform.checkBox13.Checked.ToString();
            sw.WriteLine(data);
            data = "Command_9" + "," + Program.mainform.textBox18.Text + "," + Program.mainform.checkBox14.Checked.ToString();
            sw.WriteLine(data);
            //------------------------------------command area
            data = "SE_version" + "," + Program.mainform.checkBox3.Checked.ToString() + "," + Program.mainform.textBox14.Text;
            sw.WriteLine(data);
            data = "MCU_version" + "," + Program.mainform.checkBox4.Checked.ToString() + "," + Program.mainform.textBox15.Text;
            sw.WriteLine(data);
            data = "Sensor_version" + "," + Program.mainform.checkBox5.Checked.ToString() + "," + Program.mainform.textBox17.Text;
            sw.WriteLine(data);
            //-------------------------------------version area
            data = "Image_format" + "," + Program.mainform.comboBox10.SelectedIndex.ToString();
            sw.WriteLine(data);
            //--------------------------------------Image show area

            //--------------------------------------SE area
            sw.Close();
            fs.Close();
            PS_to_DBB("Config file saved successfully.\r\n");
        }


    }
}
