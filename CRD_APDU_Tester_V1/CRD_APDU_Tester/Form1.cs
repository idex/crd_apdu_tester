﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text;
using System.Threading;

using System.Windows.Forms;
using System.IO;
using System.Security;
using System.Runtime.InteropServices;
using System.Diagnostics;



namespace CRD_APDU_Tester
{


    public partial class Form1 : Form
    {

        static OpenFileDialog OFD1 = new OpenFileDialog();
        private  void initial_combobox([In] List<string> list, [Out] ComboBox boxname)
        {
            boxname.DataSource = list;
        }
        private  void initialize_combbox()
        {
            Common.CMD_list_m1 = new List<string>(Common.CMD_list.ToArray());
            Common.CMD_list_m2 = new List<string>(Common.CMD_list.ToArray());
            Common.CMD_list_m3 = new List<string>(Common.CMD_list.ToArray());
            Common.CMD_list_m4 = new List<string>(Common.CMD_list.ToArray());
            Common.CMD_list_m5 = new List<string>(Common.CMD_list.ToArray());
            Common.CMD_list_m6 = new List<string>(Common.CMD_list.ToArray());
            Common.CMD_list_m7 = new List<string>(Common.CMD_list.ToArray());
            Common.CMD_list_m8 = new List<string>(Common.CMD_list.ToArray());//SE_list
            Common.CMD_list_m9 = new List<string>(Common.SE_list.ToArray());//SE_list
            Common.Format_list_m1 = new List<string>(Common.Format_list.ToArray());
            initial_combobox(Common.CMD_list_m1, comboBox1);
            initial_combobox(Common.CMD_list_m2, comboBox2);
            initial_combobox(Common.CMD_list_m3, comboBox3);
            initial_combobox(Common.CMD_list_m4, comboBox4);
            initial_combobox(Common.CMD_list_m5, comboBox5);
            initial_combobox(Common.CMD_list_m6, comboBox6);
            initial_combobox(Common.CMD_list_m7, comboBox7);
            initial_combobox(Common.CMD_list_m8, comboBox8);
            //initial_combobox(Common.CMD_list_m9, comboBox9);
            initial_combobox(Common.Format_list_m1, comboBox10);
        }
        public Form1()
        {
            InitializeComponent();
            //Application.DoEvents();
            
            initialize_combbox();

            //System.Timers.Timer timer = new System.Timers.Timer();
            //timer.Enabled = true;
            //timer.Interval = 100; //执行间隔时间,单位为毫秒;
            //timer.Start();
            //timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_irq);

        }

        public void timer_irq(object source, System.Timers.ElapsedEventArgs e)
        {
            //Application.DoEvents();
        }



        public void clear_debugbox()
        {
            textBox4.Text = "";
            textBox4.SelectionStart = textBox4.Text.Length;
            textBox4.ScrollToCaret();
        }




        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string about_string;
            string name_string = "Software: CRD Tester\r\n";
            string version_string = "Version:  V0.1\r\n";
            string author_string = "Author:   Howard Wang\r\n";
            string date_string = "Date:     DEC.24,2019\r\n";
            about_string = name_string + version_string + date_string + author_string;
            MessageBox.Show(about_string);
        }

        private void openUpdatebinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OFD1.Filter = "Bin Files(*.bin)|*.bin|All files (*.*)|*.*";  // "CSV Files (.csv) |*.csv|All Files (*.*)|*.*"; 
            OFD1.FilterIndex = 1;
            OFD1.Multiselect = false;
            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = OFD1.ShowDialog();
            if (userClickedOK == DialogResult.OK)
            {
                textBox6.Text = OFD1.FileNames[0];    // current path;
                Common.update_bin_filename = OFD1.FileNames[0];
            }
        }
        private void openMCUFWbinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OFD1.Filter = "Bin Files(*.bin)|*.bin|All files (*.*)|*.*";  // "CSV Files (.csv) |*.csv|All Files (*.*)|*.*"; 
            OFD1.FilterIndex = 1;
            OFD1.Multiselect = false;
            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = OFD1.ShowDialog();
            if (userClickedOK == DialogResult.OK)
            {
                textBox7.Text = OFD1.FileNames[0];    // current path;
                Common.mcu_fw_filename = OFD1.FileNames[0];
                Common.get_hash(Common.mcu_fw_filename, Common.mcu_fw_hash);
                //textBox1.Text = byte_to_hexstr(mcu_fw_hash, 16);
                print_string_to_debugbox("MCU FW hash:" + Common.byte_to_hexstr(Common.mcu_fw_hash, 16)+"\r\n");
            }
        }

        private void openSEFWbinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OFD1.Filter = "Txt Files(*.txt)|*.txt|All files (*.*)|*.*";  // "CSV Files (.csv) |*.csv|All Files (*.*)|*.*"; 
            OFD1.FilterIndex = 1;
            OFD1.Multiselect = false;
            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = OFD1.ShowDialog();
            if (userClickedOK == DialogResult.OK)
            {
                textBox5.Text = OFD1.FileNames[0];    // current path;
                Common.SE_FW_filename = OFD1.FileNames[0];
            }
        }

        private void openSensorPriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OFD1.Filter = "Bin Files(*.bin)|*.bin|All files (*.*)|*.*";  // "CSV Files (.csv) |*.csv|All Files (*.*)|*.*"; 
            OFD1.FilterIndex = 1;
            OFD1.Multiselect = false;
            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = OFD1.ShowDialog();
            if (userClickedOK == DialogResult.OK)
            {
                textBox8.Text = OFD1.FileNames[0];    // current path;
                Common.sensor_pri_fw_filename = OFD1.FileNames[0];
                Common.get_hash(Common.sensor_pri_fw_filename, Common.sensor_pri_fw_hash);
                //textBox2.Text = byte_to_hexstr(sensor_pri_fw_hash, 16);
                print_string_to_debugbox("Sensor FW hash:" + Common.byte_to_hexstr(Common.sensor_pri_fw_hash, 16) + "\r\n");
            }
        }

        private void openSensorUtibinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OFD1.Filter = "Bin Files(*.bin)|*.bin|All files (*.*)|*.*";  // "CSV Files (.csv) |*.csv|All Files (*.*)|*.*"; 
            OFD1.FilterIndex = 1;
            OFD1.Multiselect = false;
            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = OFD1.ShowDialog();
            if (userClickedOK == DialogResult.OK)
            {
                textBox9.Text = OFD1.FileNames[0];    // current path;
                Common.sensor_uti_fw_filename = OFD1.FileNames[0];
                Common.get_hash(Common.sensor_uti_fw_filename, Common.sensor_uti_fw_hash);
                //textBox3.Text = byte_to_hexstr(sensor_uti_fw_hash, 16);
                print_string_to_debugbox("Sensor BL hash:" + Common.byte_to_hexstr(Common.sensor_uti_fw_hash, 16) + "\r\n");
            }
        }




        public delegate void myinvoke8();

        //THD89_CRD_Tester.Properties.Resources.NG

        public void initial_UI()
        {
            if ((textBox4.InvokeRequired)||(label10.InvokeRequired))
            {
                myinvoke8 mi = new myinvoke8(initial_UI);
                Invoke(mi);
            }
            else
            {
                clear_all_lamp();
                clear_version_info();
            }
        }

        public void clear_all_lamp()
        {
            label10.Image = CRD_APDU_Tester.Properties.Resources.NA;
            label11.Image = CRD_APDU_Tester.Properties.Resources.NA;
            label12.Image = CRD_APDU_Tester.Properties.Resources.NA;
            label13.Image = CRD_APDU_Tester.Properties.Resources.NA;
            label14.Image = CRD_APDU_Tester.Properties.Resources.NA;
            label15.Image = CRD_APDU_Tester.Properties.Resources.NA;
            label16.Image = CRD_APDU_Tester.Properties.Resources.NA;
            label17.Image = CRD_APDU_Tester.Properties.Resources.NA;
            label18.Image = CRD_APDU_Tester.Properties.Resources.NA;
            label19.Image = CRD_APDU_Tester.Properties.Resources.NG;

        }
        public void clear_version_info()
        {
            textBox4.Text = "";
            textBox12.Text = "";
            textBox13.Text = "";
            textBox16.Text = "";

        }




        private void button1_Click(object sender, EventArgs e)
        {
            OFD1.Filter = "Bmp Files(*.bmp)|*.bmp|All files (*.*)|*.*";  // "CSV Files (.csv) |*.csv|All Files (*.*)|*.*"; 
            OFD1.FilterIndex = 1;
            OFD1.Multiselect = false;
            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = OFD1.ShowDialog();
            if (userClickedOK == DialogResult.OK)
            {
                textBox10.Text = OFD1.FileNames[0];    // current path;
                Common.load_image_filename = OFD1.FileNames[0];
            }
        }






        public Bitmap picture_3_bg = new Bitmap(264, 18);
        public Bitmap picture_2_bg = new Bitmap(545, 18);
        public Bitmap picture_4_bg = new Bitmap(545, 18);
        public Bitmap picture_5_bg = new Bitmap(545, 18);
        public Bitmap picture_6_bg = new Bitmap(545, 18);
        public Bitmap picture_7_bg = new Bitmap(545, 18);

        public void update_any_pgb(byte number, int percent_value)
        {
            switch (number)
            {
                case 2:
                    { update_picturebox_ui(ref pictureBox2, ref picture_2_bg, percent_value); }
                    break;
                case 3:
                    { update_picturebox_ui(ref pictureBox3, ref picture_3_bg, percent_value); }
                    break;
                case 4:
                    { update_picturebox_ui(ref pictureBox4, ref picture_4_bg, percent_value); }
                    break;
                case 5:
                    { update_picturebox_ui(ref pictureBox5, ref picture_5_bg, percent_value); }
                    break;
                case 6:
                    { update_picturebox_ui(ref pictureBox6, ref picture_6_bg, percent_value); }
                    break;
                case 7:
                    { update_picturebox_ui(ref pictureBox7, ref picture_7_bg, percent_value); }
                    break;
                default:
                    { }
                    break;

            }


        }
        public delegate void myinvoke5(ref PictureBox pb, ref Bitmap bm, int pct_value);

        public void update_picturebox_ui(ref PictureBox pb, ref Bitmap bm, int pct_value)
        {
            if ((groupBox8.InvokeRequired) || (pictureBox1.InvokeRequired))
            {
                myinvoke5 mi = new myinvoke5(update_picturebox_ui);
                Invoke(mi, new object[] { pb, bm, pct_value });
            }
            else
            {
                update_progressbar(ref pb, ref bm, pct_value);
            }
        }
        public void update_progressbar(ref PictureBox pb, ref Bitmap bm, int percent_value)
        {

            Graphics g = Graphics.FromImage(bm);
            g.Clear(pb.BackColor);
            //消除锯齿
            g.SmoothingMode = SmoothingMode.AntiAlias;
            //高质量，低速度绘制
            g.CompositingQuality = CompositingQuality.HighQuality;
            Font f = new Font("宋体", 15, FontStyle.Bold | FontStyle.Italic);
            float esize = 15;
            FontFamily ff = new FontFamily("宋体");
            StringFormat mySF = new StringFormat();
            StringFormat format = StringFormat.GenericDefault;
            mySF.Alignment = StringAlignment.Near;
            mySF.LineAlignment = StringAlignment.Center;
            mySF.FormatFlags = StringFormatFlags.NoWrap;
            string PercentStr = percent_value.ToString() + "%";
            SizeF strSize = g.MeasureString(PercentStr, f, new PointF(), mySF);
            int strWidth = (int)(strSize.Width + 10);

            GraphicsPath myPath = new GraphicsPath();
            int fontStyle = (int)FontStyle.Bold;
            int x = (int)((pb.Width - strWidth) / 2);
            int y = (int)((pb.Height - strSize.Height) / 2);
            Point origin = new Point(x, y + 1);

            // Add the string to the path.
            myPath.AddString(PercentStr, ff, fontStyle, esize, origin, format);
            GraphicsPath stringPath = myPath;
            int CurrentX = (pb.Width * percent_value / 100);
            Rectangle blotoutRect = new Rectangle(0, 0, CurrentX, pb.Height);
            myPath.AddRectangle(blotoutRect);
            //Draw the path to the screen.
            g.FillPath(Brushes.Green, myPath);
            pb.Image = bm;
            pb.Refresh();
        }







        public int is_start_test = 0;
        private void CMD_Start_Click(object sender, EventArgs e)
        {
            if (0 == is_start_test)
            {
                is_start_test = 1;
                Thread np = new Thread(new ThreadStart(Common.start_selected_test));
                np.IsBackground = true;
                np.Start();

            }
            else
            {
                print_string_to_debugbox("The testing is still running, please wait\r\n");
            }

        }












        public delegate void myinvoke( Label lb,  Bitmap im);
        public void change_label_image( Label lb, Bitmap im)
        {
            if (lb.InvokeRequired)
            {
                myinvoke mi = new myinvoke(change_label_image);
                Invoke(mi,new object[] {lb,im });
            }
            else
            {
                lb.Image = im;
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            string template_num_string = textBox11.Text;
            byte template_number;
            if ("" == template_num_string)
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                return;
            }
            try
            {
                template_number = Convert.ToByte(template_num_string.Substring(0, 1), 10);
            }
            catch (Exception ex)
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                print_string_to_debugbox(ex.Message + "\r\n");
                return;
            }
            if (0 == template_number) template_number = 1;
            if (6 < template_number) template_number = 6;
            if (6> template_number)
            {
                template_number += 1;
            }
            else
            {
                template_number = 1;
            }
            template_num_string = template_number.ToString();
            textBox11.Text = template_num_string;
        }
        private void button5_Click_1(object sender, EventArgs e)
        {
            string template_num_string = textBox11.Text;
            byte template_number;
            if  ("" == template_num_string)
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                return;
            }
            try
            {
                template_number = Convert.ToByte(template_num_string.Substring(0, 1), 10);
            }
            catch (Exception ex)
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                print_string_to_debugbox(ex.Message + "\r\n");
                return;
            }
            if (0 == template_number) template_number = 1;
            if (6 < template_number) template_number = 6;
            if (1< template_number)
            {
                template_number -= 1;
            }
            else
            {
                template_number = 6;
            }
            template_num_string = template_number.ToString();
            textBox11.Text = template_num_string;
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            string finger_num_string = textBox19.Text;
            byte finger_number;

            if ("" == finger_num_string) 
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                return;
            }
            try
            {
                finger_number = Convert.ToByte(finger_num_string.Substring(0, 1), 10);
            }
            catch (Exception ex)
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                print_string_to_debugbox(ex.Message + "\r\n");
                return;
            }
            if (0 == finger_number) finger_number = 1;
            if (2 < finger_number) finger_number = 2;
            if (2 == finger_number)
            {
                finger_number = 1;
            }
            else
            {
                finger_number = 2;
            }
            finger_num_string = finger_number.ToString();
            textBox19.Text = finger_num_string;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            string finger_num_string = textBox19.Text;
            byte finger_number;
            if ("" == finger_num_string) 
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                return;
            }
            try
            {
                finger_number = Convert.ToByte(finger_num_string.Substring(0, 1), 10);
            }
            catch (Exception ex)
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                print_string_to_debugbox(ex.Message + "\r\n");
                return;
            }
            if (0 == finger_number) finger_number = 1;
            if (2 < finger_number) finger_number = 2;
            if (2 == finger_number)
            {
                finger_number = 1;
            }
            else
            {
                finger_number = 2;
            }
            finger_num_string = finger_number.ToString();
            textBox19.Text = finger_num_string;
        }

        private void button5_Click(object sender, EventArgs e)
        {

            string finger_num_string = textBox19.Text;
            string template_num_string = textBox11.Text;
            byte finger_number;
            byte template_number;
            if (("" == finger_num_string) || ("" == template_num_string))
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                return;
            }
            try
            {
                finger_number = Convert.ToByte(finger_num_string.Substring(0, 1), 10);
                template_number = Convert.ToByte(template_num_string.Substring(0, 1), 10);
            }
            catch (Exception ex)
            {
                print_string_to_debugbox("Appoint data is not correct.\r\n");
                print_string_to_debugbox(ex.Message + "\r\n");
                return;
            }
            if (0 == finger_number) finger_number = 1;
            if (2 < finger_number) finger_number = 2;
            if (0 == template_number) finger_number = 1;
            if (1 < template_number)
            {
                template_number -= 1;
            }
            else
            {
                if (1 >= finger_number) return;
                finger_number -= 1;
                template_number = 6;
            }
            finger_num_string = finger_number.ToString();
            template_num_string = template_number.ToString();
            textBox19.Text = finger_num_string;
            textBox11.Text = template_num_string;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Process.GetCurrentProcess().Kill();
        }

       
   
        public string default_cfg_csv_name = "config.csv";
        public string default_cfg_path = "";
  



        private void button3_Click(object sender, EventArgs e)
        {
            if (""==textBox1.Text)
            {
                default_cfg_path = System.Environment.CurrentDirectory + "\\" + default_cfg_csv_name;
                textBox1.Text = default_cfg_path;
            }
            else
            {
                default_cfg_path = textBox1.Text;
            }
            Common.save_config_file(default_cfg_path);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if ("" == textBox1.Text)
            {
                default_cfg_path = System.Environment.CurrentDirectory + "\\" + default_cfg_csv_name;
                textBox1.Text = default_cfg_path;

            }
            else
            {
                default_cfg_path = textBox1.Text;
            }
            Common.load_config_file(default_cfg_path);
        }


        public delegate void Flushclient();
        public  void print_string_to_debugbox(string ptr_string)
        {
            Common.textbox4_new_string = ptr_string;
            MethodInvoker mi = new MethodInvoker(update_print_data);
            Invoke(mi);

        }
        public  void update_print_data()
        {
            if (textBox4.InvokeRequired)
            {
                Flushclient fc = new Flushclient(update_print_data);
                Invoke(fc);
            }
            else
            {

                textBox4.Text += Common.textbox4_new_string;
                textBox4.SelectionStart = textBox4.Text.Length;
                textBox4.ScrollToCaret();
                textBox4.Update();

            }
        }




        private void button6_Click(object sender, EventArgs e)
        {
            OFD1.Filter = "Csv Files(*.csv)|*.csv|All files (*.*)|*.*";  // "CSV Files (.csv) |*.csv|All Files (*.*)|*.*"; 
            OFD1.FilterIndex = 1;
            OFD1.Multiselect = false;
            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = OFD1.ShowDialog();
            if (userClickedOK == DialogResult.OK)
            {
                textBox1.Text = OFD1.FileNames[0];    // current path;
            }
        }

        public UInt16 FM_FW_CRC_DATA=0;

        private void FM_CRC16(byte[] data_array, int length)
        {
            int i;
            byte chBlock;
            int wCrc;
            int ch;

            wCrc = 0x6363;
            i = length;

            for(i=0;i< length;i++)
            {
                chBlock = data_array[i];
                ch = chBlock;
                ch = (ch ^ (wCrc & 0x000000FF));
                ch = (ch ^ (ch << 4));
                ch &= 0x000000ff;
                wCrc = (wCrc >> 8) ^ (ch << 8)^ (ch << 3)^ (ch >> 4);
                wCrc &= 0x0000ffff;
            }
            //Console.WriteLine(wCrc.ToString("X4"));
            FM_FW_CRC_DATA = Convert.ToUInt16(wCrc);
        }


        private void button7_Click(object sender, EventArgs e)
        {
            string binstring = "";
            string txt_name = "";
            string temp_string = "";
            if (false == File.Exists(Common.load_FW_filename))
            {
                print_string_to_debugbox("File does not exist.\r\n");
                return;
            }
            //Console.WriteLine(Path.GetDirectoryName(Common.load_FW_filename));
            txt_name = Path.GetDirectoryName(Common.load_FW_filename);
            txt_name += "\\" + "FM1280_demo" + ".txt";
            if (true == File.Exists(txt_name))
            {
                File.Delete(txt_name);
            }
            StreamReader HexReader = new StreamReader(Common.load_FW_filename);
            while (true)
            {
                string linestring = HexReader.ReadLine();
                if (null == linestring) break;
                if (":"==linestring.Substring(0,1))
                {
                    if (linestring.Substring(1, 8) == "00000001") break;
                    if (linestring.Substring(1, 1) == "1")
                    {
                        binstring += linestring.Substring(9, linestring.Length - 11);
                    }
                }

            }
            int rest_length = binstring.Length % 4096;
            int i = 0;
            int j = 0;
            for (i=rest_length;i<4096;i++)
            {
                binstring += "f";
            }
            rest_length = binstring.Length;
            //Console.WriteLine(rest_length.ToString("x8"));
            byte[] fw_data_byte = new byte[rest_length/2];
            for (i=0;i< rest_length/2;i++)
            {
                temp_string = binstring.Substring(i*2,2);
                fw_data_byte[i] = Convert.ToByte(temp_string, 16);
            }
            //Console.WriteLine(fw_data_byte[0]);
            //Console.WriteLine(fw_data_byte[2048]);
            //Console.WriteLine("crc:");
            FM_CRC16(fw_data_byte, fw_data_byte.Length);


            FileStream fs = new FileStream(txt_name, System.IO.FileMode.Create, System.IO.FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
            i = binstring.Length;
            sw.WriteLine((binstring.Length/2).ToString("X8"));
            sw.WriteLine(FM_FW_CRC_DATA.ToString("X4"));
            temp_string = "F002FF0000";//write head
            sw.WriteLine(temp_string);
            string page_start_address = "";
            string page_end_address = "";
            while (0 < i)
            {
                //earse page
                if (0 == (j % 4096))
                {
                    page_start_address = (j/2).ToString("x8");
                    page_end_address=((j/2)+0x7ff).ToString("x8");
                    temp_string = "F004000008" + page_start_address + page_end_address;
                    sw.WriteLine(temp_string);
                }
                temp_string = "F006000084"+ (j/2).ToString("x8") + binstring.Substring(j, 256);
                sw.WriteLine(temp_string);
                j += 256;
                i -= 256;
            }
            sw.Close();
            fs.Close();
            HexReader.Close();
            //Console.WriteLine(rest_length);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            OFD1.Filter = "Hex Files(*.hex)|*.hex|All files (*.*)|*.*";  // "CSV Files (.csv) |*.csv|All Files (*.*)|*.*"; 
            OFD1.FilterIndex = 1;
            OFD1.Multiselect = false;
            // Call the ShowDialog method to show the dialog box.
            DialogResult userClickedOK = OFD1.ShowDialog();
            if (userClickedOK == DialogResult.OK)
            {
                textBox2.Text = OFD1.FileNames[0];    // current path;
                Common.load_FW_filename = OFD1.FileNames[0];
            }
        }
    }
}
